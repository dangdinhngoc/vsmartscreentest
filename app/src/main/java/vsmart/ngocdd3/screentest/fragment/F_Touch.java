package vsmart.ngocdd3.screentest.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import vsmart.ngocdd3.screentest.R;
import vsmart.ngocdd3.screentest.activity.touch.A_Double_Tap_Success_Rate;
import vsmart.ngocdd3.screentest.activity.touch.A_Move_Latency;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class F_Touch extends Fragment {

    private int mPage;
    private String mTitle;
    private ListView listView;

    public F_Touch() {
        // Required empty public constructor
    }

    /**
     * Create only one instance of fragment
     *
     * @param page
     * @param title
     * @return
     */
    public static F_Touch newInstance(int page, String title) {
        F_Touch touchFragment = new F_Touch();
        Bundle args = new Bundle();
        args.putInt("page", page);
        args.putString("title", title);
        touchFragment.setArguments(args);
        return touchFragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPage = getArguments().getInt("page", 0);
        mTitle = getArguments().getString("title");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_touch, container, false);


        //We have our list view
        listView = rootView.findViewById(R.id.touch_listview);

        //Create an array of elements
        final ArrayList<String> listItems = new ArrayList<>();
        listItems.add("Double Tap Success Rate");
        listItems.add("Move Latency");
        listItems.add("");
        listItems.add("");


        //Create adapter for ArrayList
        final ArrayAdapter adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, listItems);

        //Insert Adapter into List
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {

                String selectedItem = listItems.get(position);

                if (selectedItem.equals("Double Tap Success Rate")) {
                    Intent intent = new Intent(getContext(), A_Double_Tap_Success_Rate.class);
                    startActivity(intent);
                }
                if (selectedItem.equals("Move Latency")) {
                    Intent intent = new Intent(getContext(), A_Move_Latency.class);
                    startActivity(intent);
                }


            }
        });


        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
