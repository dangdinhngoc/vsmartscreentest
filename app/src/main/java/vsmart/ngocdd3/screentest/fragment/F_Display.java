package vsmart.ngocdd3.screentest.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import vsmart.ngocdd3.screentest.R;
import vsmart.ngocdd3.screentest.activity.display.A_APL;
import vsmart.ngocdd3.screentest.activity.display.A_Auto_Brightness;
import vsmart.ngocdd3.screentest.activity.display.A_Auto_GrayScale;
import vsmart.ngocdd3.screentest.activity.display.A_Border_Scan;
import vsmart.ngocdd3.screentest.activity.display.A_CABC_function;
import vsmart.ngocdd3.screentest.activity.display.A_Crosstalk;
import vsmart.ngocdd3.screentest.activity.display.A_Custom_Color;
import vsmart.ngocdd3.screentest.activity.display.A_Flicker;
import vsmart.ngocdd3.screentest.activity.display.A_Gradient;
import vsmart.ngocdd3.screentest.activity.display.A_Manual_GrayScale;
import vsmart.ngocdd3.screentest.activity.display.A_Oled_Life_Time_Black;
import vsmart.ngocdd3.screentest.activity.display.A_Oled_Life_Time_White;
import vsmart.ngocdd3.screentest.activity.display.A_Primary_Color;
import vsmart.ngocdd3.screentest.activity.display.A_Pure_Color;
import vsmart.ngocdd3.screentest.activity.display.A_Stress_Test;
import vsmart.ngocdd3.screentest.activity.display.A_Sharpness;


/**
 * A simple {@link Fragment} subclass.
 */
public class F_Display extends Fragment {

    private int mPage;
    ListView listView;

    public F_Display() {
        // Required empty public constructor
    }

    /**
     * Create only one instance of fragment
     *
     * @param page
     * @param title
     * @return
     */
    public static F_Display newInstance(int page, String title) {
        F_Display contactsFragment = new F_Display();
        Bundle args = new Bundle();
        args.putInt("page", page);
        args.putString("title", title);
        contactsFragment.setArguments(args);
        return contactsFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPage = getArguments().getInt("page", 0);
//        mTitle = getArguments().getString("title");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_display, container, false);

        //We have our list view
        listView = rootView.findViewById(R.id.display_listview);

        //Create an array of elements
        final ArrayList<String> listItems = new ArrayList<>();
        listItems.add("Pure Color");
        listItems.add("Primary Color");
        listItems.add("Custom Color");
        listItems.add("Border Scan");
        listItems.add("Manual GrayScale");
        listItems.add("Gradient");
        listItems.add("CrossTalk");
        listItems.add("OLED Life Time White");
        listItems.add("OLED Life Time Black");
        listItems.add("Sharpness");
        listItems.add("Auto GrayScale");
        listItems.add("Flicker");
        listItems.add("Auto Brightness");
        listItems.add("Black/White Stress Test");
        listItems.add("Stress Test");
        listItems.add("APL");
        listItems.add("CABC");
        listItems.add("");
        listItems.add("");

        //Create adapter for ArrayList
        final ArrayAdapter adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, listItems);

        //Insert Adapter into List
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {

                String selectedItem = listItems.get(position);

                if (selectedItem.equals("Pure Color")) {
                    Intent intent = new Intent(getContext(), A_Pure_Color.class);
                    startActivity(intent);
                }

                if (selectedItem.equals("Primary Color")) {
                    Intent intent = new Intent(getContext(), A_Primary_Color.class);
                    startActivity(intent);
                }

                if (selectedItem.equals("Custom Color")) {
                    Intent intent = new Intent(getContext(), A_Custom_Color.class);
                    startActivity(intent);
                }

                if (selectedItem.equals("Border Scan")) {
                    Intent intent = new Intent(getContext(), A_Border_Scan.class);
                    startActivity(intent);
                }

                if (selectedItem.equals("Manual GrayScale")) {
                    Intent intent = new Intent(getContext(), A_Manual_GrayScale.class);
                    startActivity(intent);
                }

                if (selectedItem.equals("Gradient")) {
                    Intent intent = new Intent(getContext(), A_Gradient.class);
                    startActivity(intent);
                }

                if (selectedItem.equals("CrossTalk")) {
                    Intent intent = new Intent(getContext(), A_Crosstalk.class);
                    startActivity(intent);
                }

                if (selectedItem.equals("OLED Life Time White")) {
                    Intent intent = new Intent(getContext(), A_Oled_Life_Time_White.class);
                    startActivity(intent);
                }
                if (selectedItem.equals("OLED Life Time Black")) {
                    Intent intent = new Intent(getContext(), A_Oled_Life_Time_Black.class);
                    startActivity(intent);
                }
                if (selectedItem.equals("Sharpness")) {
                    Intent intent = new Intent(getContext(), A_Sharpness.class);
                    startActivity(intent);
                }
                if (selectedItem.equals("Auto GrayScale")) {
                    Intent intent = new Intent(getContext(), A_Auto_GrayScale.class);
                    startActivity(intent);
                }
                if (selectedItem.equals("Flicker")) {
                    Intent intent = new Intent(getContext(), A_Flicker.class);
                    startActivity(intent);
                }
                if (selectedItem.equals("Auto Brightness")) {
                    Intent intent = new Intent(getContext(), A_Auto_Brightness.class);
                    startActivity(intent);
                }
                if (selectedItem.equals("Stress Test")) {
                    Intent intent = new Intent(getContext(), A_Stress_Test.class);
                    startActivity(intent);
                }
                if (selectedItem.equals("APL")) {
                    Intent intent = new Intent(getContext(), A_APL.class);
                    startActivity(intent);
                }
                if (selectedItem.equals("CABC")) {
                    Intent intent = new Intent(getContext(), A_CABC_function.class);
                    startActivity(intent);
                }

            }
        });


        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


    }
}
