package vsmart.ngocdd3.screentest.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import vsmart.ngocdd3.screentest.R;
import vsmart.ngocdd3.screentest.activity.MainActivity;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;


/**
 * A simple {@link Fragment} subclass.
 */
public class F_Home extends Fragment {

    public static final String TAG = F_Home.class.getSimpleName();
    CheckBox autoMaximumBrightness, cb_white, cb_black, cb_red, cb_green, cb_blue;
    private int mPage;
    private String mTitle;
    private EditText txt_gammaStep;

//    private EditText mEditText;

    public F_Home() {
        // Required empty public constructor
    }

    /**
     * Create only one instance of fragment
     *
     * @param page
     * @param title
     * @return
     */
    public static F_Home newInstance(int page, String title) {
        F_Home homeFragment = new F_Home();
        Bundle args = new Bundle();
        args.putInt("page", page);
        args.putString("title", title);
        homeFragment.setArguments(args);
        return homeFragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPage = getArguments().getInt("page", 0);
        mTitle = getArguments().getString("title");

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        // init component
        autoMaximumBrightness = rootView.findViewById(R.id.cb_autoMaximumBrightness);
        txt_gammaStep = rootView.findViewById(R.id.txt_Step);
        cb_white = rootView.findViewById(R.id.cb_white);
        cb_black = rootView.findViewById(R.id.cb_black);
        cb_red = rootView.findViewById(R.id.cb_red);
        cb_green = rootView.findViewById(R.id.cb_green);
        cb_blue = rootView.findViewById(R.id.cb_blue);

        // prevent touch event to other fragment
        rootView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        getCheckBoxEvents();
        return rootView;

    }

    public void getCheckBoxEvents() {

        // get gamma step value
        txt_gammaStep.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    MainActivity.txt_Step = Integer.parseInt(txt_gammaStep.getText().toString());
                } catch (Exception e) {

                }
            }
        });


        _COlOR |= _RED |= _WHITE |= _BLACK |= _GREEN |= _BLUE;

        //get cb white value
        cb_white.setTag("White");
        cb_white.setOnCheckedChangeListener(onCheckedChangeListener);
        //get cb black value
        cb_black.setTag("Black");
        cb_black.setOnCheckedChangeListener(onCheckedChangeListener);

        //get cb red value
        cb_red.setTag("Red");
        cb_red.setOnCheckedChangeListener(onCheckedChangeListener);

        //get cb green value
        cb_green.setTag("Green");
        cb_green.setOnCheckedChangeListener(onCheckedChangeListener);

        //get cb blue value
        cb_blue.setTag("Blue");
        cb_blue.setOnCheckedChangeListener(onCheckedChangeListener);


        //get autoMaximumBrightness value
        autoMaximumBrightness.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                MainActivity.autoBrightness = autoMaximumBrightness.isChecked();
            }
        });
    } // End of getCheckBoxEvents

    public static int _COlOR = 0;
    static int _RED = 1;
    static int _BLUE = 2;
    static int _GREEN = 4;
    static int _BLACK = 8;
    static int _WHITE = 16;
    public static int[] LIST_COLOR = {_RED, _BLUE, _GREEN, _BLACK, _WHITE};


    CompoundButton.OnCheckedChangeListener onCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            switch ((String) buttonView.getTag()) {
                case "White":
                    if (isChecked) _COlOR |= 16;
                    else _COlOR ^= 16;
                    break;
                case "Black":
                    if (isChecked) _COlOR |= 8;
                    else _COlOR ^= 8;
                    break;
                case "Green":
                    if (isChecked) _COlOR |= 4;
                    else _COlOR ^= 4;
                    break;
                case "Blue":
                    if (isChecked) _COlOR |= 2;
                    else _COlOR ^= 2;
                    break;
                case "Red":
                    if (isChecked) _COlOR |= 1;
                    else _COlOR ^= 1;
                    break;
            }

            Log.d(TAG, "COLOR: " + _COlOR);
        }
    };


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
//        String text = mEditText.getText().toString().trim();
//        if (text != null) {
//            outState.putString("text", text);
//        }
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null) {
            String text_old = savedInstanceState.getString("text");
//            mEditText.setText(text_old);
        }
    }


}
