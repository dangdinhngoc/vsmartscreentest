package vsmart.ngocdd3.screentest.layout;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Handler;
import android.view.View;


public class L_GrayScale extends View {

    int i = 0;
    final Handler handler = new Handler();

    public L_GrayScale(Context context) {
        super( context );
    }

    @Override
    protected void onDraw(final Canvas canvas) {
        super.onDraw( canvas );


        handler.postDelayed( new Runnable() {
            @Override
            public void run() {
                if (i > 255) {
                    i = 255;
                    setBackgroundColor( Color.rgb( i, i, i ) );
                } else {
                    setBackgroundColor( Color.rgb( i, i, i ) );
                    i = i + 1;
                }
                if (i == 255) {
                    i = 0;
                }
            }
        }, 200 );


    }


}
