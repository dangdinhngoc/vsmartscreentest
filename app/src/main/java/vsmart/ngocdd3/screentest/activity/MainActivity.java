package vsmart.ngocdd3.screentest.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.WindowManager;

import vsmart.ngocdd3.screentest.R;
import vsmart.ngocdd3.screentest.services.S_DepthPageTransformer;
import vsmart.ngocdd3.screentest.services.S_MyViewPagerAdapter;
import vsmart.ngocdd3.screentest.services.S_Services;

public class MainActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    public static boolean autoBrightness = true;
    public static int txt_Step;
    public static boolean cb_white = true, cb_black = true, cb_red = true, cb_green = true, cb_blue = true, serviceState = true;
    Intent myService;

    @SuppressLint("WifiManagerLeak")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // find view
        mToolbar = findViewById(R.id.toolbar);
        mTabLayout = findViewById(R.id.tabs);
        mViewPager = findViewById(R.id.viewpager);

        // set screen always on
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        //
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Vsmart Screen Test");
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.WRITE_SETTINGS},
                1);
        setupViewPager();
        mTabLayout.setupWithViewPager(mViewPager);

        myService = new Intent(this, S_Services.class);


        if (ContextCompat.checkSelfPermission(
                this, Manifest.permission.WRITE_SETTINGS) ==
                PackageManager.PERMISSION_GRANTED) {
            // do somethings.

            // turn off adaptive brightness
            Settings.System.putInt(getContentResolver(),
                    Settings.System.SCREEN_BRIGHTNESS_MODE, Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);

        }


    }// End Oncreate


    /**
     * Setup ViewPager
     */
    private void setupViewPager() {
        S_MyViewPagerAdapter adapter = new S_MyViewPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(adapter);
        mViewPager.setCurrentItem(0);
        mViewPager.setPageTransformer(true, new S_DepthPageTransformer());
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = new MenuInflater(this);
        menuInflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.item01:
                setdialog();
            case R.id.item02:
                if (serviceState) {
                    startService(myService);
                    serviceState = false;
                } else {

                    stopService(myService);
                    serviceState = true;
                }


        }
        return super.onOptionsItemSelected(item);
    }

    // get about information
    private void setdialog() {
        Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.about);
        dialog.show();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService(myService);
    }
}


