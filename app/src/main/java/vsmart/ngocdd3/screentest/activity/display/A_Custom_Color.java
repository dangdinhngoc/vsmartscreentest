package vsmart.ngocdd3.screentest.activity.display;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import vsmart.ngocdd3.screentest.R;
import vsmart.ngocdd3.screentest.activity.MainActivity;
import vsmart.ngocdd3.screentest.interfaces.I_MainFunctions;


public class A_Custom_Color extends AppCompatActivity implements I_MainFunctions {
    View custom_Color_Layout;
    int r = 0;
    int g = 0;
    int b = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__custom__color);
        custom_Color_Layout = findViewById(R.id.custom_Color_Layout);

        setFullScreen();
        dialogSetRGB();

    }// end oncreate

    private void setBackground(int r, int g, int b) {
        custom_Color_Layout.setBackgroundColor(Color.rgb(r, g, b));
    }


    private void dialogSetRGB() {
        Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.custom_color_dialog);

        EditText red_value = (EditText) dialog.findViewById(R.id.txt_red_value);

        EditText green_value = (EditText) dialog.findViewById(R.id.txt_green_value);

        EditText blue_value = (EditText) dialog.findViewById(R.id.txt_blue_value);

        Button OK = dialog.findViewById(R.id.btnOK);
        Button Cancel = dialog.findViewById(R.id.btnCancel);

        OK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (red_value.getText().toString().equals("")) {
                    r = 0;

                } else {
                    r = Integer.parseInt(red_value.getText().toString());
                }

                if (green_value.getText().toString().equals("")) {
                    g = 0;
                } else {
                    g = Integer.parseInt(green_value.getText().toString());
                }

                if (blue_value.getText().toString().equals("")) {
                    b = 0;
                } else {
                    b = Integer.parseInt(blue_value.getText().toString());
                }


                setBackground(r, g, b);
                dialog.dismiss();
            }
        });
        Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getActionMasked();

        switch (action) {
            case MotionEvent.ACTION_DOWN:
                break;
            case MotionEvent.ACTION_MOVE:

                break;
            case MotionEvent.ACTION_UP:
                setFullScreen();

            case MotionEvent.ACTION_CANCEL:
                // Return a VelocityTracker object back to be re-used by others.
                break;
        }
        return true;
    }


    @Override
    public void setFullScreen() {
        // init variables
        WindowManager.LayoutParams params = getWindow().getAttributes();
        View mDecor = this.getWindow().getDecorView();


        // set maximum brightness
        if (MainActivity.autoBrightness) {
            params.screenBrightness = WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_FULL;
        }

        // set screen always on
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // set full screen
        mDecor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                | View.SYSTEM_UI_FLAG_IMMERSIVE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            params.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
        }
        getWindow().setAttributes(params);
    }
}