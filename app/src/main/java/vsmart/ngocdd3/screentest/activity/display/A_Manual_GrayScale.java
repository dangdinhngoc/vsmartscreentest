package vsmart.ngocdd3.screentest.activity.display;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.TextView;
import android.widget.Toast;


import vsmart.ngocdd3.screentest.R;
import vsmart.ngocdd3.screentest.activity.MainActivity;
import vsmart.ngocdd3.screentest.interfaces.I_MainFunctions;

public class A_Manual_GrayScale extends AppCompatActivity implements I_MainFunctions {
    private View layout_grascale;
    private TextView gray_value;
    int grayStep = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_greyscale);

        layout_grascale = findViewById(R.id.layout_grayscale);
        gray_value = findViewById(R.id.gray_value);

        setFullScreen();
        layout_grascale.setBackgroundColor(Color.rgb(grayStep, grayStep, grayStep));
        gray_value.setText(grayStep + "");

        // set expcetion
        if (MainActivity.txt_Step == 0) {
            MainActivity.txt_Step = 4;
        }

        // set gratscale
        setGreyScale();
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getActionMasked();

        switch (action) {
            case MotionEvent.ACTION_DOWN:
                break;
            case MotionEvent.ACTION_MOVE:

                break;
            case MotionEvent.ACTION_UP:
                setFullScreen();

            case MotionEvent.ACTION_CANCEL:
                // Return a VelocityTracker object back to be re-used by others.
                break;
        }
        return true;
    }


    private void setGreyScale() {
        // long tap to reduce gray level
        layout_grascale.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                setFullScreen();

                grayStep -= MainActivity.txt_Step;
                if (grayStep < 0) {
                    grayStep = 0;
                }

                layout_grascale.setBackgroundColor(Color.rgb(grayStep, grayStep, grayStep));
                gray_value.setText(grayStep + "");
                return true;
            }
        });

        // tap to increase gray level
        layout_grascale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFullScreen();

                grayStep += MainActivity.txt_Step;
                if (grayStep > 255) {
                    grayStep = 255;
                }

                layout_grascale.setBackgroundColor(Color.rgb(grayStep, grayStep, grayStep));
                gray_value.setText(grayStep + "");
            }
        });
    }


    @Override
    public void setFullScreen() {
        // init variables
        WindowManager.LayoutParams params = getWindow().getAttributes();
        View mDecor = this.getWindow().getDecorView();


        // set maximum brightness
        if (MainActivity.autoBrightness) {
            params.screenBrightness = WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_FULL;
        }

        // set screen always on
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // set full screen
        mDecor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                | View.SYSTEM_UI_FLAG_IMMERSIVE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            params.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
        }
        getWindow().setAttributes(params);
    }

}