package vsmart.ngocdd3.screentest.activity.display;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;


import vsmart.ngocdd3.screentest.R;
import vsmart.ngocdd3.screentest.activity.MainActivity;
import vsmart.ngocdd3.screentest.interfaces.I_MainFunctions;

import static vsmart.ngocdd3.screentest.R.drawable.blue_gradient_angle_0;
import static vsmart.ngocdd3.screentest.R.drawable.blue_gradient_angle_180;
import static vsmart.ngocdd3.screentest.R.drawable.blue_gradient_angle_270;
import static vsmart.ngocdd3.screentest.R.drawable.blue_gradient_angle_360;
import static vsmart.ngocdd3.screentest.R.drawable.blue_gradient_angle_90;
import static vsmart.ngocdd3.screentest.R.drawable.green_gradient_angle_0;
import static vsmart.ngocdd3.screentest.R.drawable.green_gradient_angle_180;
import static vsmart.ngocdd3.screentest.R.drawable.green_gradient_angle_270;
import static vsmart.ngocdd3.screentest.R.drawable.green_gradient_angle_360;
import static vsmart.ngocdd3.screentest.R.drawable.green_gradient_angle_90;
import static vsmart.ngocdd3.screentest.R.drawable.red_gradient_angle_0;
import static vsmart.ngocdd3.screentest.R.drawable.red_gradient_angle_180;
import static vsmart.ngocdd3.screentest.R.drawable.red_gradient_angle_270;
import static vsmart.ngocdd3.screentest.R.drawable.red_gradient_angle_360;
import static vsmart.ngocdd3.screentest.R.drawable.red_gradient_angle_90;
import static vsmart.ngocdd3.screentest.R.drawable.white_gradient_angle_0;
import static vsmart.ngocdd3.screentest.R.drawable.white_gradient_angle_180;
import static vsmart.ngocdd3.screentest.R.drawable.white_gradient_angle_270;
import static vsmart.ngocdd3.screentest.R.drawable.white_gradient_angle_360;
import static vsmart.ngocdd3.screentest.R.drawable.white_gradient_angle_90;

public class A_Gradient extends AppCompatActivity implements I_MainFunctions {

    private ImageView gradient;
    int count = 1;
    float start_Point, end_Point;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gradient);

        gradient = findViewById(R.id.gradient_imageview);

        setFullScreen();

    }// end of onCreate

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int index = event.getActionIndex();
        int action = event.getActionMasked();

        switch (action) {
            case MotionEvent.ACTION_DOWN:
                start_Point = event.getX(index);

                break;
            case MotionEvent.ACTION_MOVE:

                break;
            case MotionEvent.ACTION_UP:
                end_Point = event.getX(index);

                if ((start_Point - end_Point) > 50) {
                    count++;
                    setGradient();
                }
                if ((start_Point - end_Point) < 50) {
                    count--;
                    setGradient();
                }
                setFullScreen();
            case MotionEvent.ACTION_CANCEL:
                // Return a VelocityTracker object back to be re-used by others.
//                mVelocityTracker.recycle();
                break;
        }
        return true;
    }


    private void setGradient() {
        switch (count) {
            case 1:
                gradient.setImageDrawable(getResources().getDrawable(white_gradient_angle_0));
//                count += 1;
                break;
            case 2:
                gradient.setImageDrawable(getResources().getDrawable(white_gradient_angle_90));
//                count += 1;
                break;
            case 3:
                gradient.setImageDrawable(getResources().getDrawable(white_gradient_angle_180));
//                count += 1;
                break;
            case 4:
                gradient.setImageDrawable(getResources().getDrawable(white_gradient_angle_270));
//                count += 1;
                break;
            case 5:
                gradient.setImageDrawable(getResources().getDrawable(white_gradient_angle_360));
//                count += 1;
                break;
            case 6:
                gradient.setImageDrawable(getResources().getDrawable(red_gradient_angle_0));
//                count += 1;
                break;
            case 7:
                gradient.setImageDrawable(getResources().getDrawable(red_gradient_angle_90));
//                count += 1;
                break;
            case 8:
                gradient.setImageDrawable(getResources().getDrawable(red_gradient_angle_180));
//                count += 1;
                break;
            case 9:
                gradient.setImageDrawable(getResources().getDrawable(red_gradient_angle_270));
//                count += 1;
                break;
            case 10:
                gradient.setImageDrawable(getResources().getDrawable(red_gradient_angle_360));
//                count += 1;
                break;
            case 11:
                gradient.setImageDrawable(getResources().getDrawable(green_gradient_angle_0));
//                count += 1;
                break;
            case 12:
                gradient.setImageDrawable(getResources().getDrawable(green_gradient_angle_90));
//                count += 1;
                break;
            case 13:
                gradient.setImageDrawable(getResources().getDrawable(green_gradient_angle_180));
//                count += 1;
                break;
            case 14:
                gradient.setImageDrawable(getResources().getDrawable(green_gradient_angle_270));
//                count += 1;
                break;
            case 15:
                gradient.setImageDrawable(getResources().getDrawable(green_gradient_angle_360));
//                count += 1;
                break;
            case 16:
                gradient.setImageDrawable(getResources().getDrawable(blue_gradient_angle_0));
//                count += 1;
                break;
            case 17:
                gradient.setImageDrawable(getResources().getDrawable(blue_gradient_angle_90));
//                count += 1;
                break;
            case 18:
                gradient.setImageDrawable(getResources().getDrawable(blue_gradient_angle_180));
//                count += 1;
                break;
            case 19:
                gradient.setImageDrawable(getResources().getDrawable(blue_gradient_angle_270));
//                count += 1;
                break;
            case 20:
                gradient.setImageDrawable(getResources().getDrawable(blue_gradient_angle_360));
//                count = 1;
                break;

        }
    }

    @Override
    public void setFullScreen() {
        // init variables
        WindowManager.LayoutParams params = getWindow().getAttributes();
        View mDecor = this.getWindow().getDecorView();


        // set maximum brightness
        if (MainActivity.autoBrightness) {
            params.screenBrightness = WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_FULL;
        }

        // set screen always on
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // set full screen
        mDecor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                | View.SYSTEM_UI_FLAG_IMMERSIVE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            params.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
        }
        getWindow().setAttributes(params);
    }


}