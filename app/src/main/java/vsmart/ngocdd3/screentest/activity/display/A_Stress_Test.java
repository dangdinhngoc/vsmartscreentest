package vsmart.ngocdd3.screentest.activity.display;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import vsmart.ngocdd3.screentest.R;
import vsmart.ngocdd3.screentest.activity.MainActivity;
import vsmart.ngocdd3.screentest.fragment.F_Home;
import vsmart.ngocdd3.screentest.interfaces.I_MainFunctions;

public class A_Stress_Test extends AppCompatActivity implements I_MainFunctions {

    TextView textView_screenstresstest;
    View mDecor;
    WindowManager.LayoutParams params;
    Boolean cb_white, cb_black, cb_red, cb_green, cb_blue;
    boolean breakLoop = false;
    Thread t;
    int delay;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_screenstresstest);

        //intit
        textView_screenstresstest = findViewById(R.id.textView_screenstresstest);

        cb_white = MainActivity.cb_white;
        cb_black = MainActivity.cb_black;
        cb_red = MainActivity.cb_red;
        cb_green = MainActivity.cb_green;
        cb_blue = MainActivity.cb_blue;
        mCOLORS = F_Home._COlOR;

        delay = MainActivity.txt_Step;

        // set full screen view
        setFullScreen();
        myColorHandler = new MyColorHandler();
        changeColor();
    }// end Oncreate


    int mCOLORS;


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    final int _RED = 1;
    final int _BLUE = 2;
    final int _GREEN = 4;
    final int _BLACK = 8;
    final int _WHITE = 16;

    public void colorsChange(int color) {

        Message msg = new Message();

        switch (color) {
            case _WHITE:
                msg.what = _WHITE;
                break;
            case _BLACK:
                msg.what = _BLACK;
                break;
            case _RED:
                msg.what = _RED;
                break;
            case _GREEN:
                msg.what = _GREEN;
                break;
            case _BLUE:
                msg.what = _BLUE;
                break;
        }

        myColorHandler.sendMessage(msg);

    }

    private MyColorHandler myColorHandler;

    class MyColorHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            int color = msg.what;
            switch (color) {
                case _WHITE:
                    textView_screenstresstest.setBackgroundColor(Color.rgb(255, 255, 255));
                    break;
                case _BLACK:
                    textView_screenstresstest.setBackgroundColor(Color.rgb(0, 0, 0));
                    break;
                case _RED:
                    textView_screenstresstest.setBackgroundColor(Color.rgb(255, 0, 0));
                    break;
                case _GREEN:
                    textView_screenstresstest.setBackgroundColor(Color.rgb(0, 255, 0));
                    break;
                case _BLUE:
                    textView_screenstresstest.setBackgroundColor(Color.rgb(0, 0, 255));
                    break;
            }
        }
    }


    void changeColor() {

        t = new Thread(new Runnable() {
            @Override
            public void run() {
                int[] listColor_selected = F_Home.LIST_COLOR;

                while (true && !breakLoop) {
                    for (int color : listColor_selected) {
                        if ((mCOLORS & color) == color) {
//                    publishProgress(color);
                            colorsChange(color);


                            try {

                                if (delay < 0) {
                                    delay = 1000;
                                }
                                Thread.sleep(delay);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        });
        t.start();

    }


    @Override
    protected void onResume() {
        super.onResume();
        changeColor();
    }

    @Override
    protected void onPause() {
        super.onPause();
        t.destroy();

    }

    @Override
    public void setFullScreen() {
        // init variables
        params = getWindow().getAttributes();
        mDecor = this.getWindow().getDecorView();

        // set maximum brightness
        if (MainActivity.autoBrightness) {
            params.screenBrightness = WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_FULL;
        }

        // set screen always on
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // set full screen
        mDecor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                | View.SYSTEM_UI_FLAG_IMMERSIVE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            params.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
        }
        getWindow().setAttributes(params);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getActionMasked();

        switch (action) {
            case MotionEvent.ACTION_DOWN:
                break;
            case MotionEvent.ACTION_MOVE:

                break;
            case MotionEvent.ACTION_UP:
                setFullScreen();

            case MotionEvent.ACTION_CANCEL:
                // Return a VelocityTracker object back to be re-used by others.
                break;
        }
        return true;
    }
}