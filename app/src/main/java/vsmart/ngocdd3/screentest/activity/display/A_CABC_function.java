package vsmart.ngocdd3.screentest.activity.display;

import android.annotation.SuppressLint;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

import vsmart.ngocdd3.screentest.R;
import vsmart.ngocdd3.screentest.interfaces.I_MainFunctions;

public class A_CABC_function extends AppCompatActivity implements I_MainFunctions {

    WindowManager.LayoutParams params;
    public static View content_1, content_2, content_3, content_4, content_5, content_6, content_7, content_8, content_9, content_10, content_11, content_12, content_13, content_14, content_15, content_16, content_17, content_18, content_19, content_20, content_21, content_22, content_23, content_24;
    boolean run = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__c_a_b_c_function);

        params = getWindow().getAttributes();

// init
        content_1 = findViewById(R.id.content_1);
        content_2 = findViewById(R.id.content_2);
        content_3 = findViewById(R.id.content_3);
        content_4 = findViewById(R.id.content_4);
        content_5 = findViewById(R.id.content_5);
        content_6 = findViewById(R.id.content_6);
        content_7 = findViewById(R.id.content_7);
        content_8 = findViewById(R.id.content_8);
        content_9 = findViewById(R.id.content_9);
        content_10 = findViewById(R.id.content_10);
        content_11 = findViewById(R.id.content_11);
        content_12 = findViewById(R.id.content_12);
        content_13 = findViewById(R.id.content_13);
        content_14 = findViewById(R.id.content_14);
        content_15 = findViewById(R.id.content_15);
        content_16 = findViewById(R.id.content_16);
        content_17 = findViewById(R.id.content_17);
        content_18 = findViewById(R.id.content_18);
        content_19 = findViewById(R.id.content_19);
        content_20 = findViewById(R.id.content_20);
        content_21 = findViewById(R.id.content_21);
        content_22 = findViewById(R.id.content_22);
        content_23 = findViewById(R.id.content_23);
        content_24 = findViewById(R.id.content_24);
        setFullScreen();
        background_service();

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getActionMasked();

        switch (action) {
            case MotionEvent.ACTION_DOWN:
                break;
            case MotionEvent.ACTION_MOVE:

                break;
            case MotionEvent.ACTION_UP:
                setFullScreen();

            case MotionEvent.ACTION_CANCEL:
                // Return a VelocityTracker object back to be re-used by others.
                break;
        }
        return true;
    }

    @Override
    public void setFullScreen() {
        // init variables

        View mDecor = this.getWindow().getDecorView();


        // set maximum brightness
//        if (MainActivity.autoBrightness) {
//            params.screenBrightness = WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_FULL;
//        }

        // set screen always on
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // set full screen
        mDecor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                | View.SYSTEM_UI_FLAG_IMMERSIVE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            params.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
        }
        getWindow().setAttributes(params);
    }


    @SuppressLint("StaticFieldLeak")
    private void background_service() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                if (run) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        setContentColor(0, 300);
                    }
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        setContentColor(1, 0);
                    }
                }
            }
        }).start();


    }

    private void sleep(int time) {

        System.out.println("Sleeping");
        try {
            Thread.sleep(time);
            System.out.println("Done");
        } catch (InterruptedException e) {
            System.out.println(e + "");
            e.printStackTrace();
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void setContentColor(int color, int timedelay) {

        if (color == 0) {
            content_1.setBackgroundColor(getColor(R.color.black));
            sleep(timedelay);
            content_2.setBackgroundColor(getColor(R.color.black));
            sleep(timedelay);
            content_3.setBackgroundColor(getColor(R.color.black));
            sleep(timedelay);
            content_4.setBackgroundColor(getColor(R.color.black));
            sleep(timedelay);
            content_5.setBackgroundColor(getColor(R.color.black));
            sleep(timedelay);
            content_6.setBackgroundColor(getColor(R.color.black));
            sleep(timedelay);
            content_7.setBackgroundColor(getColor(R.color.black));
            sleep(timedelay);
            content_8.setBackgroundColor(getColor(R.color.black));
            sleep(timedelay);
            content_9.setBackgroundColor(getColor(R.color.black));
            sleep(timedelay);
            content_10.setBackgroundColor(getColor(R.color.black));
            sleep(timedelay);
            content_11.setBackgroundColor(getColor(R.color.black));
            sleep(timedelay);
            content_12.setBackgroundColor(getColor(R.color.black));
            sleep(timedelay);
            content_13.setBackgroundColor(getColor(R.color.black));
            sleep(timedelay);
            content_14.setBackgroundColor(getColor(R.color.black));
            sleep(timedelay);
            content_15.setBackgroundColor(getColor(R.color.black));
            sleep(timedelay);
            content_16.setBackgroundColor(getColor(R.color.black));
            sleep(timedelay);
            content_17.setBackgroundColor(getColor(R.color.black));
            sleep(timedelay);
            content_18.setBackgroundColor(getColor(R.color.black));
            sleep(timedelay);
            content_19.setBackgroundColor(getColor(R.color.black));
            sleep(timedelay);
            content_20.setBackgroundColor(getColor(R.color.black));
            sleep(timedelay);
            content_21.setBackgroundColor(getColor(R.color.black));
            sleep(timedelay);
            content_22.setBackgroundColor(getColor(R.color.black));
            sleep(timedelay);
            content_23.setBackgroundColor(getColor(R.color.black));
            sleep(timedelay);
            content_24.setBackgroundColor(getColor(R.color.black));

        } else {
            content_1.setBackgroundColor(getColor(R.color.white));
            sleep(timedelay);
            content_2.setBackgroundColor(getColor(R.color.white));
            sleep(timedelay);
            content_3.setBackgroundColor(getColor(R.color.white));
            sleep(timedelay);
            content_4.setBackgroundColor(getColor(R.color.white));
            sleep(timedelay);
            content_5.setBackgroundColor(getColor(R.color.white));
            sleep(timedelay);
            content_6.setBackgroundColor(getColor(R.color.white));
            sleep(timedelay);
            content_7.setBackgroundColor(getColor(R.color.white));
            sleep(timedelay);
            content_8.setBackgroundColor(getColor(R.color.white));
            sleep(timedelay);
            content_9.setBackgroundColor(getColor(R.color.white));
            sleep(timedelay);
            content_10.setBackgroundColor(getColor(R.color.white));
            sleep(timedelay);
            content_11.setBackgroundColor(getColor(R.color.white));
            sleep(timedelay);
            content_12.setBackgroundColor(getColor(R.color.white));
            sleep(timedelay);
            content_13.setBackgroundColor(getColor(R.color.white));
            sleep(timedelay);
            content_14.setBackgroundColor(getColor(R.color.white));
            sleep(timedelay);
            content_15.setBackgroundColor(getColor(R.color.white));
            sleep(timedelay);
            content_16.setBackgroundColor(getColor(R.color.white));
            sleep(timedelay);
            content_17.setBackgroundColor(getColor(R.color.white));
            sleep(timedelay);
            content_18.setBackgroundColor(getColor(R.color.white));
            sleep(timedelay);
            content_19.setBackgroundColor(getColor(R.color.white));
            sleep(timedelay);
            content_20.setBackgroundColor(getColor(R.color.white));
            sleep(timedelay);
            content_21.setBackgroundColor(getColor(R.color.white));
            sleep(timedelay);
            content_22.setBackgroundColor(getColor(R.color.white));
            sleep(timedelay);
            content_23.setBackgroundColor(getColor(R.color.white));
            sleep(timedelay);
            content_24.setBackgroundColor(getColor(R.color.white));
            sleep(timedelay);
        }


    }

}