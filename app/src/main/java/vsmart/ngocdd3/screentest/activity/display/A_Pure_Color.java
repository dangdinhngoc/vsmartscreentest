package vsmart.ngocdd3.screentest.activity.display;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import vsmart.ngocdd3.screentest.R;
import vsmart.ngocdd3.screentest.activity.MainActivity;
import vsmart.ngocdd3.screentest.interfaces.I_MainFunctions;


public class A_Pure_Color extends AppCompatActivity implements I_MainFunctions {

    private View view_Colors;
    int pure_Colors = 1;

    WindowManager.LayoutParams params;
    View mDecor;
    Toast toast;
    GestureDetector gestureDetector;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_colors);

        // init...
        params = getWindow().getAttributes();
        view_Colors = findViewById(R.id.view_colors);
        toast = new Toast(getApplicationContext());
        mDecor = this.getWindow().getDecorView();

        // set full screen and keep screen on
        setFullScreen();

        view_Colors.setBackgroundColor(Color.rgb(255, 255, 255));
    } // end of oncreate


    private void set_Color() {
        switch (pure_Colors) {
            case 1:
                view_Colors.setBackgroundColor(Color.rgb(255, 0, 0));

                //change case number
                pure_Colors += 1;
                break;
            case 2:
                view_Colors.setBackgroundColor(Color.rgb(0, 255, 0));

                //change case number
                pure_Colors += 1;
                break;
            case 3:
                view_Colors.setBackgroundColor(Color.rgb(0, 0, 255));

                //change case number
                pure_Colors += 1;
                break;
            case 4:
                view_Colors.setBackgroundColor(Color.rgb(255, 255, 255));

                //change case number
                pure_Colors += 1;
                break;
            case 5:
                view_Colors.setBackgroundColor(Color.rgb(0, 0, 0));

                //change case number
                pure_Colors = 1;
                break;
        }
    }


    // auto full screen again after stop activity
    @Override
    public void onResume() {
        super.onResume();
        setFullScreen();
    }

    @Override
    public void setFullScreen() {
        // init variables

        View mDecor = this.getWindow().getDecorView();


        // set maximum brightness
        if (MainActivity.autoBrightness) {
            params.screenBrightness = WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_FULL;
        }

        // set screen always on
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // set full screen
        mDecor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                | View.SYSTEM_UI_FLAG_IMMERSIVE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            params.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
        }
        getWindow().setAttributes(params);
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getActionMasked();

        switch (action) {
            case MotionEvent.ACTION_DOWN:
                set_Color();
                break;
            case MotionEvent.ACTION_MOVE:

                break;
            case MotionEvent.ACTION_UP:
                setFullScreen();

            case MotionEvent.ACTION_CANCEL:
                // Return a VelocityTracker object back to be re-used by others.
                break;
        }
        return true;
    }

}