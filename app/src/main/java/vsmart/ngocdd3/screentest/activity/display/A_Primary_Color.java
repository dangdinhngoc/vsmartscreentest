package vsmart.ngocdd3.screentest.activity.display;

import android.graphics.Color;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;


import vsmart.ngocdd3.screentest.R;
import vsmart.ngocdd3.screentest.activity.MainActivity;
import vsmart.ngocdd3.screentest.interfaces.I_MainFunctions;

public class A_Primary_Color extends AppCompatActivity implements I_MainFunctions {

    private View view_Colors;
    int primary_Colors = 1;
    int screen_Width, screen_Height;
    Toast toast;
    WindowManager.LayoutParams params;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_colors);

        // init...
        view_Colors = findViewById(R.id.view_colors);
        toast = new Toast(getApplicationContext());
        params = getWindow().getAttributes();

        // set full screen and keep screen on
        setFullScreen();
        getScreenResolution();
    }// end oncreate

    private void getScreenResolution() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getRealSize(size);
            screen_Width = size.x;
            screen_Height = size.y;
        }
    }


    private void set_Color() {
        switch (primary_Colors) {
            case 1:
                view_Colors.setBackgroundColor(Color.rgb(255, 0, 0));
                toast.cancel();
                toast = Toast.makeText(getApplicationContext(), "Red", Toast.LENGTH_SHORT);
                toast.show();

                //change case number
                primary_Colors += 1;
                break;
            case 2:
                view_Colors.setBackgroundColor(Color.rgb(0, 255, 0));
                toast.cancel();
                toast = Toast.makeText(getApplicationContext(), "Green", Toast.LENGTH_SHORT);
                toast.show();

                //change case number
                primary_Colors += 1;
                break;
            case 3:
                view_Colors.setBackgroundColor(Color.rgb(0, 0, 255));
                toast.cancel();
                toast = Toast.makeText(getApplicationContext(), "Blue", Toast.LENGTH_SHORT);
                toast.show();

                //change case number
                primary_Colors += 1;
                break;
            case 4:
                view_Colors.setBackgroundColor(Color.rgb(255, 255, 255));
                toast.cancel();
                toast = Toast.makeText(getApplicationContext(), "White", Toast.LENGTH_SHORT);
                toast.show();

                //change case number
                primary_Colors += 1;
                break;
            case 5:
                view_Colors.setBackgroundColor(Color.rgb(0, 0, 0));
                toast.cancel();
                toast = Toast.makeText(getApplicationContext(), "Black", Toast.LENGTH_SHORT);
                toast.show();

                //change case number
                primary_Colors += 1;
                break;

            case 6:
                view_Colors.setBackgroundColor(Color.rgb(255, 255, 0));
                toast.cancel();
                toast = Toast.makeText(getApplicationContext(), "Yello", Toast.LENGTH_SHORT);
                toast.show();

                //change case number
                primary_Colors += 1;
                break;

            case 7:
                view_Colors.setBackgroundColor(Color.rgb(0, 255, 255));
                toast.cancel();
                toast = Toast.makeText(getApplicationContext(), "Cyan / Aqua", Toast.LENGTH_SHORT);
                toast.show();

                //change case number
                primary_Colors += 1;
                break;
            case 8:
                view_Colors.setBackgroundColor(Color.rgb(255, 0, 255));
                toast.cancel();
                toast = Toast.makeText(getApplicationContext(), "Magenta / Fuchsia", Toast.LENGTH_SHORT);
                toast.show();

                //change case number
                primary_Colors += 1;
                break;
            case 9:
                view_Colors.setBackgroundColor(Color.rgb(192, 192, 192));
                toast.cancel();
                toast = Toast.makeText(getApplicationContext(), "Sliver", Toast.LENGTH_SHORT);
                toast.show();

                //change case number
                primary_Colors += 1;
                break;
            case 10:
                view_Colors.setBackgroundColor(Color.rgb(128, 128, 128));
                toast.cancel();
                toast = Toast.makeText(getApplicationContext(), "Gray 128", Toast.LENGTH_SHORT);
                toast.show();

                //change case number
                primary_Colors += 1;
                break;
            case 11:
                view_Colors.setBackgroundColor(Color.rgb(128, 0, 0));
                toast.cancel();
                toast = Toast.makeText(getApplicationContext(), "Maroon", Toast.LENGTH_SHORT);
                toast.show();

                //change case number
                primary_Colors += 1;
                break;
            case 12:
                view_Colors.setBackgroundColor(Color.rgb(128, 128, 0));
                toast.cancel();
                toast = Toast.makeText(getApplicationContext(), "Olive", Toast.LENGTH_SHORT);
                toast.show();

                //change case number
                primary_Colors += 1;
                break;
            case 13:
                view_Colors.setBackgroundColor(Color.rgb(0, 128, 0));
                toast.cancel();
                toast = Toast.makeText(getApplicationContext(), "Lime", Toast.LENGTH_SHORT);
                toast.show();

                //change case number
                primary_Colors += 1;
                break;
            case 14:
                view_Colors.setBackgroundColor(Color.rgb(128, 0, 128));
                toast.cancel();
                toast = Toast.makeText(getApplicationContext(), "Purple", Toast.LENGTH_SHORT);
                toast.show();

                //change case number
                primary_Colors += 1;
                break;
            case 15:
                view_Colors.setBackgroundColor(Color.rgb(0, 128, 128));
                toast.cancel();
                toast = Toast.makeText(getApplicationContext(), "Teal", Toast.LENGTH_SHORT);
                toast.show();

                //change case number
                primary_Colors += 1;
                break;
            case 16:
                view_Colors.setBackgroundColor(Color.rgb(0, 0, 128));
                toast.cancel();
                toast = Toast.makeText(getApplicationContext(), "Navy", Toast.LENGTH_SHORT);
                toast.show();

                //change case number
                primary_Colors += 1;
                break;

            case 17:
                view_Colors.setBackgroundColor(Color.rgb(87, 87, 87));
                toast.cancel();
                toast = Toast.makeText(getApplicationContext(), "Gray 87", Toast.LENGTH_SHORT);
                toast.show();

                //change case number
                primary_Colors += 1;
                break;
            case 18:
                view_Colors.setBackgroundColor(Color.rgb(64, 64, 64));
                toast.cancel();
                toast = Toast.makeText(getApplicationContext(), "Gray 64", Toast.LENGTH_SHORT);
                toast.show();

                //change case number
                primary_Colors = 1;
                break;
        }
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int index = event.getActionIndex();
        int action = event.getActionMasked();

        switch (action) {
            case MotionEvent.ACTION_DOWN:
                set_Color();
                break;
            case MotionEvent.ACTION_MOVE:

                break;
            case MotionEvent.ACTION_UP:
                setFullScreen();

            case MotionEvent.ACTION_CANCEL:
                // Return a VelocityTracker object back to be re-used by others.
                break;
        }
        return true;
    }


    @Override
    public void setFullScreen() {
        // init variables
        View mDecor = this.getWindow().getDecorView();


        // set maximum brightness
        if (MainActivity.autoBrightness) {
            params.screenBrightness = WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_FULL;
        }

        // set screen always on
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // set full screen
        mDecor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                | View.SYSTEM_UI_FLAG_IMMERSIVE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            params.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
        }
        getWindow().setAttributes(params);
    }
}