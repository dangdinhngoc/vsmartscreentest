package vsmart.ngocdd3.screentest.activity.touch;

import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;


import java.util.ArrayList;

import vsmart.ngocdd3.screentest.R;
import vsmart.ngocdd3.screentest.activity.MainActivity;


public class A_Move_Latency extends AppCompatActivity {

    private ConstraintLayout main_background;
    int max_X, max_Y, touches_Counter = 0;
    Toast toast;
    View mDecor;
    WindowManager.LayoutParams params;
    ArrayList<Integer> touchlist_X, touchlist_Y;
    TextView txt_touch_id_1, txt_touch_id_2, txt_touch_id_3, txt_touch_id_4, txt_touch_id_5,
            txt_touch_id_6, txt_touch_id_7, txt_touch_id_8, txt_touch_id_9, txt_touch_id_0,
            txt_action_down, txt_action_up, txt_count_touches_points, txt_jump_pixel, touch_couter_label;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_move_latency);
        params = getWindow().getAttributes();

        // Set full screen view
        set_FullScreen();

        // init layout
        main_background = findViewById(R.id.main_background);
        txt_action_down = findViewById(R.id.txt_action_down);
        txt_action_up = findViewById(R.id.txt_action_up);
        txt_count_touches_points = findViewById(R.id.txt_count_touches_points);
        txt_touch_id_0 = findViewById(R.id.txt_touch_id_1);
        txt_touch_id_1 = findViewById(R.id.txt_touch_id_2);
        txt_touch_id_2 = findViewById(R.id.txt_touch_id_3);
        txt_touch_id_3 = findViewById(R.id.txt_touch_id_4);
        txt_touch_id_4 = findViewById(R.id.txt_touch_id_5);
        txt_touch_id_5 = findViewById(R.id.txt_touch_id_6);
        txt_touch_id_6 = findViewById(R.id.txt_touch_id_7);
        txt_touch_id_7 = findViewById(R.id.txt_touch_id_8);
        txt_touch_id_8 = findViewById(R.id.txt_touch_id_9);
        txt_touch_id_9 = findViewById(R.id.txt_touch_id_10);
        txt_jump_pixel = findViewById(R.id.txt_jump_pixel);
        touch_couter_label = findViewById(R.id.touch_couter);


        // init variables
        touchlist_X = new ArrayList<Integer>();
        touchlist_Y = new ArrayList<Integer>();

        set_FullScreen();
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {

        // variables
        int X = (int) event.getX();
        int Y = (int) event.getY();
        int eventAction = event.getAction();
        int touchCounter = event.getPointerCount();

        // show text
        touchlist_X.add(X);
        touchlist_Y.add(Y);
        txt_count_touches_points.setText("POINTS: " + touchCounter);

        switch (eventAction) {
            case MotionEvent.ACTION_DOWN:
                max_X = 0;
                max_Y = 0;
                txt_jump_pixel.setText("");
                txt_action_down.setText("DOWN: " + X + " - " + Y + " |");
                set_FullScreen();
                break;

            case MotionEvent.ACTION_MOVE:
                set_FullScreen();

                if (touchCounter == 1) {

                    int point_x0 = (int) event.getX(event.findPointerIndex(event.getPointerId(0)));
                    int point_y0 = (int) event.getY(event.findPointerIndex(event.getPointerId(0)));

                    txt_touch_id_0.setText(event.getPointerId(0) + ": X: " + point_x0 + "  Y: " + point_y0);
                    txt_touch_id_1.setText("");
                    txt_touch_id_2.setText("");
                    txt_touch_id_3.setText("");
                    txt_touch_id_4.setText("");
                    txt_touch_id_5.setText("");
                    txt_touch_id_6.setText("");
                    txt_touch_id_7.setText("");
                    txt_touch_id_8.setText("");
                    txt_touch_id_9.setText("");
                }
                if (touchCounter == 2) {
                    int point_x0 = (int) event.getX(event.findPointerIndex(event.getPointerId(0)));
                    int point_y0 = (int) event.getY(event.findPointerIndex(event.getPointerId(0)));

                    int point_x1 = (int) event.getX(event.findPointerIndex(event.getPointerId(1)));
                    int point_y1 = (int) event.getY(event.findPointerIndex(event.getPointerId(1)));

                    txt_touch_id_0.setText(event.getPointerId(0) + ": " + point_x0 + "  Y: " + point_y0);
                    txt_touch_id_1.setText(event.getPointerId(1) + ": " + point_x1 + "  Y: " + point_y1);
                    txt_touch_id_2.setText("");
                    txt_touch_id_3.setText("");
                    txt_touch_id_4.setText("");
                    txt_touch_id_5.setText("");
                    txt_touch_id_6.setText("");
                    txt_touch_id_7.setText("");
                    txt_touch_id_8.setText("");
                    txt_touch_id_9.setText("");
                }
                if (touchCounter == 3) {
                    int point_x0 = (int) event.getX(event.findPointerIndex(event.getPointerId(0)));
                    int point_y0 = (int) event.getY(event.findPointerIndex(event.getPointerId(0)));

                    int point_x1 = (int) event.getX(event.findPointerIndex(event.getPointerId(1)));
                    int point_y1 = (int) event.getY(event.findPointerIndex(event.getPointerId(1)));

                    int point_x2 = (int) event.getX(event.findPointerIndex(event.getPointerId(2)));
                    int point_y2 = (int) event.getY(event.findPointerIndex(event.getPointerId(2)));

                    txt_touch_id_0.setText(event.getPointerId(0) + ": " + point_x0 + "  Y: " + point_y0);
                    txt_touch_id_1.setText(event.getPointerId(1) + ": " + point_x1 + "  Y: " + point_y1);
                    txt_touch_id_2.setText(event.getPointerId(2) + ": " + point_x2 + "  Y: " + point_y2);
                    txt_touch_id_3.setText("");
                    txt_touch_id_4.setText("");
                    txt_touch_id_5.setText("");
                    txt_touch_id_6.setText("");
                    txt_touch_id_7.setText("");
                    txt_touch_id_8.setText("");
                    txt_touch_id_9.setText("");

                }
                if (touchCounter == 4) {
                    int point_x0 = (int) event.getX(event.findPointerIndex(event.getPointerId(0)));
                    int point_y0 = (int) event.getY(event.findPointerIndex(event.getPointerId(0)));

                    int point_x1 = (int) event.getX(event.findPointerIndex(event.getPointerId(1)));
                    int point_y1 = (int) event.getY(event.findPointerIndex(event.getPointerId(1)));

                    int point_x2 = (int) event.getX(event.findPointerIndex(event.getPointerId(2)));
                    int point_y2 = (int) event.getY(event.findPointerIndex(event.getPointerId(2)));


                    int point_x3 = (int) event.getX(event.findPointerIndex(event.getPointerId(3)));
                    int point_y3 = (int) event.getY(event.findPointerIndex(event.getPointerId(3)));

                    txt_touch_id_0.setText(event.getPointerId(0) + ": " + point_x0 + "  Y: " + point_y0);
                    txt_touch_id_1.setText(event.getPointerId(1) + ": " + point_x1 + "  Y: " + point_y1);
                    txt_touch_id_2.setText(event.getPointerId(2) + ": " + point_x2 + "  Y: " + point_y2);
                    txt_touch_id_3.setText(event.getPointerId(3) + ": " + point_x3 + "  Y: " + point_y3);
                    txt_touch_id_4.setText("");
                    txt_touch_id_5.setText("");
                    txt_touch_id_6.setText("");
                    txt_touch_id_7.setText("");
                    txt_touch_id_8.setText("");
                    txt_touch_id_9.setText("");

                }
                if (touchCounter == 5) {
                    int point_x0 = (int) event.getX(event.findPointerIndex(event.getPointerId(0)));
                    int point_y0 = (int) event.getY(event.findPointerIndex(event.getPointerId(0)));

                    int point_x1 = (int) event.getX(event.findPointerIndex(event.getPointerId(1)));
                    int point_y1 = (int) event.getY(event.findPointerIndex(event.getPointerId(1)));

                    int point_x2 = (int) event.getX(event.findPointerIndex(event.getPointerId(2)));
                    int point_y2 = (int) event.getY(event.findPointerIndex(event.getPointerId(2)));


                    int point_x3 = (int) event.getX(event.findPointerIndex(event.getPointerId(3)));
                    int point_y3 = (int) event.getY(event.findPointerIndex(event.getPointerId(3)));


                    int point_x4 = (int) event.getX(event.findPointerIndex(event.getPointerId(4)));
                    int point_y4 = (int) event.getY(event.findPointerIndex(event.getPointerId(4)));


                    txt_touch_id_0.setText(event.getPointerId(0) + ": " + point_x0 + "  Y: " + point_y0);
                    txt_touch_id_1.setText(event.getPointerId(1) + ": " + point_x1 + "  Y: " + point_y1);
                    txt_touch_id_2.setText(event.getPointerId(2) + ": " + point_x2 + "  Y: " + point_y2);
                    txt_touch_id_3.setText(event.getPointerId(3) + ": " + point_x3 + "  Y: " + point_y3);
                    txt_touch_id_4.setText(event.getPointerId(4) + ": " + point_x4 + "  Y: " + point_y4);
                    txt_touch_id_5.setText("");
                    txt_touch_id_6.setText("");
                    txt_touch_id_7.setText("");
                    txt_touch_id_8.setText("");
                    txt_touch_id_9.setText("");
                }
                if (touchCounter == 6) {
                    int point_x0 = (int) event.getX(event.findPointerIndex(event.getPointerId(0)));
                    int point_y0 = (int) event.getY(event.findPointerIndex(event.getPointerId(0)));

                    int point_x1 = (int) event.getX(event.findPointerIndex(event.getPointerId(1)));
                    int point_y1 = (int) event.getY(event.findPointerIndex(event.getPointerId(1)));

                    int point_x2 = (int) event.getX(event.findPointerIndex(event.getPointerId(2)));
                    int point_y2 = (int) event.getY(event.findPointerIndex(event.getPointerId(2)));


                    int point_x3 = (int) event.getX(event.findPointerIndex(event.getPointerId(3)));
                    int point_y3 = (int) event.getY(event.findPointerIndex(event.getPointerId(3)));


                    int point_x4 = (int) event.getX(event.findPointerIndex(event.getPointerId(4)));
                    int point_y4 = (int) event.getY(event.findPointerIndex(event.getPointerId(4)));

                    int point_x5 = (int) event.getX(event.findPointerIndex(event.getPointerId(5)));
                    int point_y5 = (int) event.getY(event.findPointerIndex(event.getPointerId(5)));


                    txt_touch_id_0.setText(event.getPointerId(0) + ": " + point_x0 + "  Y: " + point_y0);
                    txt_touch_id_1.setText(event.getPointerId(1) + ": " + point_x1 + "  Y: " + point_y1);
                    txt_touch_id_2.setText(event.getPointerId(2) + ": " + point_x2 + "  Y: " + point_y2);
                    txt_touch_id_3.setText(event.getPointerId(3) + ": " + point_x3 + "  Y: " + point_y3);
                    txt_touch_id_4.setText(event.getPointerId(4) + ": " + point_x4 + "  Y: " + point_y4);
                    txt_touch_id_5.setText(event.getPointerId(5) + ": " + point_x5 + "  Y: " + point_y5);
                    txt_touch_id_6.setText("");
                    txt_touch_id_7.setText("");
                    txt_touch_id_8.setText("");
                    txt_touch_id_9.setText("");
                }
                if (touchCounter == 7) {
                    int point_x0 = (int) event.getX(event.findPointerIndex(event.getPointerId(0)));
                    int point_y0 = (int) event.getY(event.findPointerIndex(event.getPointerId(0)));

                    int point_x1 = (int) event.getX(event.findPointerIndex(event.getPointerId(1)));
                    int point_y1 = (int) event.getY(event.findPointerIndex(event.getPointerId(1)));

                    int point_x2 = (int) event.getX(event.findPointerIndex(event.getPointerId(2)));
                    int point_y2 = (int) event.getY(event.findPointerIndex(event.getPointerId(2)));


                    int point_x3 = (int) event.getX(event.findPointerIndex(event.getPointerId(3)));
                    int point_y3 = (int) event.getY(event.findPointerIndex(event.getPointerId(3)));


                    int point_x4 = (int) event.getX(event.findPointerIndex(event.getPointerId(4)));
                    int point_y4 = (int) event.getY(event.findPointerIndex(event.getPointerId(4)));

                    int point_x5 = (int) event.getX(event.findPointerIndex(event.getPointerId(5)));
                    int point_y5 = (int) event.getY(event.findPointerIndex(event.getPointerId(5)));

                    int point_x6 = (int) event.getX(event.findPointerIndex(event.getPointerId(6)));
                    int point_y6 = (int) event.getY(event.findPointerIndex(event.getPointerId(6)));

                    txt_touch_id_0.setText(event.getPointerId(0) + ": " + point_x0 + "  Y: " + point_y0);
                    txt_touch_id_1.setText(event.getPointerId(1) + ": " + point_x1 + "  Y: " + point_y1);
                    txt_touch_id_2.setText(event.getPointerId(2) + ": " + point_x2 + "  Y: " + point_y2);
                    txt_touch_id_3.setText(event.getPointerId(3) + ": " + point_x3 + "  Y: " + point_y3);
                    txt_touch_id_4.setText(event.getPointerId(4) + ": " + point_x4 + "  Y: " + point_y4);
                    txt_touch_id_5.setText(event.getPointerId(5) + ": " + point_x5 + "  Y: " + point_y5);
                    txt_touch_id_6.setText(event.getPointerId(6) + ": " + point_x6 + "  Y: " + point_y6);
                    txt_touch_id_7.setText("");
                    txt_touch_id_8.setText("");
                    txt_touch_id_9.setText("");
                }
                if (touchCounter == 8) {
                    int point_x0 = (int) event.getX(event.findPointerIndex(event.getPointerId(0)));
                    int point_y0 = (int) event.getY(event.findPointerIndex(event.getPointerId(0)));

                    int point_x1 = (int) event.getX(event.findPointerIndex(event.getPointerId(1)));
                    int point_y1 = (int) event.getY(event.findPointerIndex(event.getPointerId(1)));

                    int point_x2 = (int) event.getX(event.findPointerIndex(event.getPointerId(2)));
                    int point_y2 = (int) event.getY(event.findPointerIndex(event.getPointerId(2)));


                    int point_x3 = (int) event.getX(event.findPointerIndex(event.getPointerId(3)));
                    int point_y3 = (int) event.getY(event.findPointerIndex(event.getPointerId(3)));


                    int point_x4 = (int) event.getX(event.findPointerIndex(event.getPointerId(4)));
                    int point_y4 = (int) event.getY(event.findPointerIndex(event.getPointerId(4)));

                    int point_x5 = (int) event.getX(event.findPointerIndex(event.getPointerId(5)));
                    int point_y5 = (int) event.getY(event.findPointerIndex(event.getPointerId(5)));

                    int point_x6 = (int) event.getX(event.findPointerIndex(event.getPointerId(6)));
                    int point_y6 = (int) event.getY(event.findPointerIndex(event.getPointerId(6)));

                    int point_x7 = (int) event.getX(event.findPointerIndex(event.getPointerId(7)));
                    int point_y7 = (int) event.getY(event.findPointerIndex(event.getPointerId(7)));

                    txt_touch_id_0.setText(event.getPointerId(0) + ": " + point_x0 + "  Y: " + point_y0);
                    txt_touch_id_1.setText(event.getPointerId(1) + ": " + point_x1 + "  Y: " + point_y1);
                    txt_touch_id_2.setText(event.getPointerId(2) + ": " + point_x2 + "  Y: " + point_y2);
                    txt_touch_id_3.setText(event.getPointerId(3) + ": " + point_x3 + "  Y: " + point_y3);
                    txt_touch_id_4.setText(event.getPointerId(4) + ": " + point_x4 + "  Y: " + point_y4);
                    txt_touch_id_5.setText(event.getPointerId(5) + ": " + point_x5 + "  Y: " + point_y5);
                    txt_touch_id_6.setText(event.getPointerId(6) + ": " + point_x6 + "  Y: " + point_y6);
                    txt_touch_id_7.setText(event.getPointerId(7) + ": " + point_x7 + "  Y: " + point_y7);
                    txt_touch_id_8.setText("");
                    txt_touch_id_9.setText("");
                }
                if (touchCounter == 9) {
                    int point_x0 = (int) event.getX(event.findPointerIndex(event.getPointerId(0)));
                    int point_y0 = (int) event.getY(event.findPointerIndex(event.getPointerId(0)));

                    int point_x1 = (int) event.getX(event.findPointerIndex(event.getPointerId(1)));
                    int point_y1 = (int) event.getY(event.findPointerIndex(event.getPointerId(1)));

                    int point_x2 = (int) event.getX(event.findPointerIndex(event.getPointerId(2)));
                    int point_y2 = (int) event.getY(event.findPointerIndex(event.getPointerId(2)));


                    int point_x3 = (int) event.getX(event.findPointerIndex(event.getPointerId(3)));
                    int point_y3 = (int) event.getY(event.findPointerIndex(event.getPointerId(3)));


                    int point_x4 = (int) event.getX(event.findPointerIndex(event.getPointerId(4)));
                    int point_y4 = (int) event.getY(event.findPointerIndex(event.getPointerId(4)));

                    int point_x5 = (int) event.getX(event.findPointerIndex(event.getPointerId(5)));
                    int point_y5 = (int) event.getY(event.findPointerIndex(event.getPointerId(5)));

                    int point_x6 = (int) event.getX(event.findPointerIndex(event.getPointerId(6)));
                    int point_y6 = (int) event.getY(event.findPointerIndex(event.getPointerId(6)));

                    int point_x7 = (int) event.getX(event.findPointerIndex(event.getPointerId(7)));
                    int point_y7 = (int) event.getY(event.findPointerIndex(event.getPointerId(7)));

                    int point_x8 = (int) event.getX(event.findPointerIndex(event.getPointerId(8)));
                    int point_y8 = (int) event.getY(event.findPointerIndex(event.getPointerId(8)));

                    txt_touch_id_0.setText(event.getPointerId(0) + ": " + point_x0 + "  Y: " + point_y0);
                    txt_touch_id_1.setText(event.getPointerId(1) + ": " + point_x1 + "  Y: " + point_y1);
                    txt_touch_id_2.setText(event.getPointerId(2) + ": " + point_x2 + "  Y: " + point_y2);
                    txt_touch_id_3.setText(event.getPointerId(3) + ": " + point_x3 + "  Y: " + point_y3);
                    txt_touch_id_4.setText(event.getPointerId(4) + ": " + point_x4 + "  Y: " + point_y4);
                    txt_touch_id_5.setText(event.getPointerId(5) + ": " + point_x5 + "  Y: " + point_y5);
                    txt_touch_id_6.setText(event.getPointerId(6) + ": " + point_x6 + "  Y: " + point_y6);
                    txt_touch_id_7.setText(event.getPointerId(7) + ": " + point_x7 + "  Y: " + point_y7);
                    txt_touch_id_8.setText(event.getPointerId(8) + ": " + point_x8 + "  Y: " + point_y8);
                    txt_touch_id_9.setText("");
                }
                if (touchCounter == 10) {
                    int point_x0 = (int) event.getX(event.findPointerIndex(event.getPointerId(0)));
                    int point_y0 = (int) event.getY(event.findPointerIndex(event.getPointerId(0)));

                    int point_x1 = (int) event.getX(event.findPointerIndex(event.getPointerId(1)));
                    int point_y1 = (int) event.getY(event.findPointerIndex(event.getPointerId(1)));

                    int point_x2 = (int) event.getX(event.findPointerIndex(event.getPointerId(2)));
                    int point_y2 = (int) event.getY(event.findPointerIndex(event.getPointerId(2)));


                    int point_x3 = (int) event.getX(event.findPointerIndex(event.getPointerId(3)));
                    int point_y3 = (int) event.getY(event.findPointerIndex(event.getPointerId(3)));


                    int point_x4 = (int) event.getX(event.findPointerIndex(event.getPointerId(4)));
                    int point_y4 = (int) event.getY(event.findPointerIndex(event.getPointerId(4)));

                    int point_x5 = (int) event.getX(event.findPointerIndex(event.getPointerId(5)));
                    int point_y5 = (int) event.getY(event.findPointerIndex(event.getPointerId(5)));

                    int point_x6 = (int) event.getX(event.findPointerIndex(event.getPointerId(6)));
                    int point_y6 = (int) event.getY(event.findPointerIndex(event.getPointerId(6)));

                    int point_x7 = (int) event.getX(event.findPointerIndex(event.getPointerId(7)));
                    int point_y7 = (int) event.getY(event.findPointerIndex(event.getPointerId(7)));

                    int point_x8 = (int) event.getX(event.findPointerIndex(event.getPointerId(8)));
                    int point_y8 = (int) event.getY(event.findPointerIndex(event.getPointerId(8)));

                    int point_x9 = (int) event.getX(event.findPointerIndex(event.getPointerId(9)));
                    int point_y9 = (int) event.getY(event.findPointerIndex(event.getPointerId(9)));

                    txt_touch_id_0.setText(event.getPointerId(0) + ": " + point_x0 + "  Y: " + point_y0);
                    txt_touch_id_1.setText(event.getPointerId(1) + ": " + point_x1 + "  Y: " + point_y1);
                    txt_touch_id_2.setText(event.getPointerId(2) + ": " + point_x2 + "  Y: " + point_y2);
                    txt_touch_id_3.setText(event.getPointerId(3) + ": " + point_x3 + "  Y: " + point_y3);
                    txt_touch_id_4.setText(event.getPointerId(4) + ": " + point_x4 + "  Y: " + point_y4);
                    txt_touch_id_5.setText(event.getPointerId(5) + ": " + point_x5 + "  Y: " + point_y5);
                    txt_touch_id_6.setText(event.getPointerId(6) + ": " + point_x6 + "  Y: " + point_y6);
                    txt_touch_id_7.setText(event.getPointerId(7) + ": " + point_x7 + "  Y: " + point_y7);
                    txt_touch_id_8.setText(event.getPointerId(8) + ": " + point_x8 + "  Y: " + point_y8);
                    txt_touch_id_9.setText(event.getPointerId(9) + ": " + point_x9 + "  Y: " + point_y9);
                }
                break;

            case MotionEvent.ACTION_UP:
                set_FullScreen();

                txt_action_up.setText("UP: " + X + " - " + Y + " |");
                touches_Counter++;
                touch_couter_label.setText(touches_Counter + "");

                int temp_x = 0;
                int temp_y = 0;
                for (int i = 0; i < touchlist_X.size(); i++) {
                    if (i + 1 < touchlist_X.size()) {

                        temp_x = (touchlist_X.get(i) - touchlist_X.get(i + 1));
                        temp_y = touchlist_Y.get(i) - touchlist_Y.get(i + 1);

                        if (temp_x < 0) {
                            temp_x = temp_x * -1;
                        }

                        if (temp_y < 0) {
                            temp_y = temp_y * -1;
                        }

                        if (temp_x > max_X) {
                            max_X = temp_x;
                        }
                        if (temp_y > max_Y) {
                            max_Y = temp_y;
                        }
                    }

                }

                txt_jump_pixel.setText(max_X + "|" + max_Y);
                break;

        }

        return true;
    }


    private void set_FullScreen() {
        // init variables

        mDecor = this.getWindow().getDecorView();


        // set maximum brightness
        if (MainActivity.autoBrightness) {
            params.screenBrightness = WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_FULL;
        }

        // set screen always on
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // set full screen
        mDecor.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            params.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
        }
        getWindow().setAttributes(params);
    }
}