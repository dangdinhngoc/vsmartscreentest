package vsmart.ngocdd3.screentest.activity.display;

import android.app.Dialog;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import java.lang.reflect.Method;

import vsmart.ngocdd3.screentest.R;
import vsmart.ngocdd3.screentest.activity.MainActivity;
import vsmart.ngocdd3.screentest.interfaces.I_MainFunctions;

public class A_APL extends AppCompatActivity implements I_MainFunctions {

    View apl;
    int realWidth, realHeight, padding_top, padding_left;
    float WidthAPL, HeightAPL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__a_p_l);

        apl = findViewById(R.id.apl);
        setFullScreen();

        getScreenSize();
        getDialogValue();

    }// end oncreate


    private void calculateAPL(int realWidth, int realHeight, float percentage) {
        WidthAPL = (float) (Math.pow(percentage, 0.5) * realWidth);
        HeightAPL = (float) (Math.pow(percentage, 0.5) * realHeight);

        System.out.println(WidthAPL + "W-APL");
        System.out.println(HeightAPL + "H-APL");

        padding_top = (int) ((realWidth - WidthAPL) / 2);
        padding_left = (int) ((realHeight - HeightAPL) / 2);

        System.out.println(padding_top + "padding_top");
        System.out.println(padding_left + "padding_left");
    }

    private void getDialogValue() {

        Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.apl_dialog);

        EditText apl_percentage = (EditText) dialog.findViewById(R.id.txt_apl_percentage);

        Button OK = dialog.findViewById(R.id.btnOK);
        Button Cancel = dialog.findViewById(R.id.btnCancel);

        OK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float apl_value;
                if (apl_percentage.getText().toString().equals("")) {
                    apl_value = 1;

                } else {
                    apl_value = Float.parseFloat(apl_percentage.getText().toString());
                }


                calculateAPL(realWidth, realHeight, apl_value / 100);

                apl.getLayoutParams().width = (int) WidthAPL;
                apl.getLayoutParams().height = (int) HeightAPL;
                dialog.dismiss();
            }
        });
        Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    private void getScreenSize() {

        Display display = this.getWindowManager().getDefaultDisplay();

        if (Build.VERSION.SDK_INT >= 17) {
            //new pleasant way to get real metrics
            DisplayMetrics realMetrics = new DisplayMetrics();
            display.getRealMetrics(realMetrics);
            realWidth = realMetrics.widthPixels;
            realHeight = realMetrics.heightPixels;

        } else if (Build.VERSION.SDK_INT >= 14) {
            //reflection for this weird in-between time
            try {
                Method mGetRawH = Display.class.getMethod("getRawHeight");
                Method mGetRawW = Display.class.getMethod("getRawWidth");
                realWidth = (Integer) mGetRawW.invoke(display);
                realHeight = (Integer) mGetRawH.invoke(display);
            } catch (Exception e) {
                //this may not be 100% accurate, but it's all we've got
                realWidth = display.getWidth();
                realHeight = display.getHeight();
            }

        } else {
            //This should be close, as lower API devices should not have window navigation bars
            realWidth = display.getWidth();
            realHeight = display.getHeight();
        }

        System.out.println(realWidth + "-Screen Size");
        System.out.println(realHeight + "-Screen Size");

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getActionMasked();

        switch (action) {
            case MotionEvent.ACTION_DOWN:
                break;
            case MotionEvent.ACTION_MOVE:

                break;
            case MotionEvent.ACTION_UP:
                setFullScreen();

            case MotionEvent.ACTION_CANCEL:
                // Return a VelocityTracker object back to be re-used by others.
                break;
        }
        return true;
    }

    @Override
    public void setFullScreen() {
        // init variables
        WindowManager.LayoutParams params = getWindow().getAttributes();
        View mDecor = this.getWindow().getDecorView();


        // set maximum brightness
        if (MainActivity.autoBrightness) {
            params.screenBrightness = WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_FULL;
        }

        // set screen always on
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // set full screen
        mDecor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                | View.SYSTEM_UI_FLAG_IMMERSIVE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            params.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
        }
        getWindow().setAttributes(params);
    }

}