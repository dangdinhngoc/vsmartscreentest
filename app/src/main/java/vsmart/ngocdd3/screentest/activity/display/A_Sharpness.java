package vsmart.ngocdd3.screentest.activity.display;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;


import vsmart.ngocdd3.screentest.R;
import vsmart.ngocdd3.screentest.activity.MainActivity;
import vsmart.ngocdd3.screentest.interfaces.I_MainFunctions;


public class A_Sharpness extends AppCompatActivity implements I_MainFunctions {

    private TextView txt_textview;
    int textSize = 1;
    LinearLayout mainbackground;
    Boolean sharpness = true;
    View mDecor;
    WindowManager.LayoutParams params;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sharpness);

        mainbackground = findViewById(R.id.mainbackground);
        txt_textview = findViewById(R.id.txt_textview);

        //set change text size
        changeTextSize();

        //set full screen view
        setFullScreen();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getActionMasked();

        switch (action) {
            case MotionEvent.ACTION_DOWN:
                break;
            case MotionEvent.ACTION_MOVE:

                break;
            case MotionEvent.ACTION_UP:
                setFullScreen();

            case MotionEvent.ACTION_CANCEL:
                // Return a VelocityTracker object back to be re-used by others.
                break;
        }
        return true;
    }


    private void changeTextSize() {
        txt_textview.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (sharpness) {
                    txt_textview.setTextColor(getResources().getColor(R.color.white));
                    txt_textview.setBackgroundColor(getResources().getColor(R.color.black));
                    sharpness = false;
                } else {
                    txt_textview.setTextColor(getResources().getColor(R.color.black));
                    txt_textview.setBackgroundColor(getResources().getColor(R.color.white));
                    sharpness = true;
                }
                return true;
            }
        });

        txt_textview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt_textview.setTextSize(textSize);
                textSize = textSize + 1;
            }
        });

    }


    @Override
    public void setFullScreen() {
        // init variables
        params = getWindow().getAttributes();
        mDecor = this.getWindow().getDecorView();


        // set maximum brightness
        if (MainActivity.autoBrightness) {
            params.screenBrightness = WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_FULL;
        }

        // set screen always on
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // set full screen
        mDecor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                | View.SYSTEM_UI_FLAG_IMMERSIVE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            params.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
        }
        getWindow().setAttributes(params);
    }
}