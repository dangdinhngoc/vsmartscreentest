package vsmart.ngocdd3.screentest.activity.display;

import android.graphics.Color;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;


import vsmart.ngocdd3.screentest.R;
import vsmart.ngocdd3.screentest.activity.MainActivity;
import vsmart.ngocdd3.screentest.interfaces.I_MainFunctions;

public class A_Crosstalk extends AppCompatActivity implements I_MainFunctions {


    private RelativeLayout background;
    private LinearLayout top, buttom;
    private Boolean state = true;
    int greylevel;
    int width = 0;
    int height = 0;
    int PX9mm = 0;
    int count = 1;

    //    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crosstalk);

        // init
        top = findViewById(R.id.layout_top);
        buttom = findViewById(R.id.layout_buttom);
        background = findViewById(R.id.mainbackground);
        top.setBackgroundColor(Color.rgb(0, 0, 0));
        buttom.setBackgroundColor(Color.rgb(0, 0, 0));
        greylevel = 127;
        background.setBackgroundColor(Color.rgb(greylevel, greylevel, greylevel));

        //get info
        Horizontal_Crosstalk_01();
        getInfo();
        getEvents();
        setFullScreen();

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getActionMasked();

        switch (action) {
            case MotionEvent.ACTION_DOWN:
                break;
            case MotionEvent.ACTION_MOVE:

                break;
            case MotionEvent.ACTION_UP:
                setFullScreen();

            case MotionEvent.ACTION_CANCEL:
                // Return a VelocityTracker object back to be re-used by others.
                break;
        }
        return true;
    }

    private void getEvents() {

        background.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (count) {
                    case 1:
                        Horizontal_Crosstalk_01();
                        count += 1;
                        break;
                    case 2:
                        Horizontal_Crosstalk_02();
                        count += 1;
                        break;
                    case 3:
                        Horizontal_Crosstalk_03();
                        count += 1;
                        break;
                    case 4:
                        Horizontal_Crosstalk_04();
                        count += 1;
                        break;
                    case 5:
                        Vertical_Crosstalk_01();
                        count += 1;
                        break;
                    case 6:
                        Vertical_Crosstalk_02();
                        count += 1;
                        break;
                    case 7:
                        Vertical_Crosstalk_03();
                        count += 1;
                        break;
                    case 8:
                        Vertical_Crosstalk_04();
                        count = 1;
                        break;
                }

            }
        });
    }

    private void Horizontal_Crosstalk_01() {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(width / 3, height / 3);

        layoutParams.setMargins(0, height / 2 - PX9mm, 0, 0);
        top.setLayoutParams(layoutParams);

        RelativeLayout.LayoutParams layoutParams1 = new RelativeLayout.LayoutParams(width / 3, height / 3);
        layoutParams1.setMargins(width / 3 * 2, height / 2 - PX9mm, 0, 0);
        buttom.setLayoutParams(layoutParams1);
    }

    private void Horizontal_Crosstalk_02() {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(width / 3, height / 3);

        layoutParams.setMargins(0, height / 2 + PX9mm, 0, 0);
        top.setLayoutParams(layoutParams);

        RelativeLayout.LayoutParams layoutParams1 = new RelativeLayout.LayoutParams(width / 3, height / 3);
        layoutParams1.setMargins(2 * width / 3, height / 2 + PX9mm, 0, 0);
        buttom.setLayoutParams(layoutParams1);
    }

    private void Horizontal_Crosstalk_03() {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(width / 3, height / 3);

        layoutParams.setMargins(0, height / 6 + PX9mm, 0, 0);
        top.setLayoutParams(layoutParams);

        RelativeLayout.LayoutParams layoutParams1 = new RelativeLayout.LayoutParams(width / 3, height / 3);
        layoutParams1.setMargins(2 * width / 3, height / 6 + PX9mm, 0, 0);
        buttom.setLayoutParams(layoutParams1);
    }

    private void Horizontal_Crosstalk_04() {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(width / 3, height / 3);

        layoutParams.setMargins(0, height / 6 - PX9mm, 0, 0);
        top.setLayoutParams(layoutParams);

        RelativeLayout.LayoutParams layoutParams1 = new RelativeLayout.LayoutParams(width / 3, height / 3);
        layoutParams1.setMargins(2 * width / 3, height / 6 - PX9mm, 0, 0);
        buttom.setLayoutParams(layoutParams1);
    }

    private void Vertical_Crosstalk_01() {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(width / 3, height / 3);

        layoutParams.setMargins(width / 2 - PX9mm, 0, 0, 0);
        top.setLayoutParams(layoutParams);

        RelativeLayout.LayoutParams layoutParams1 = new RelativeLayout.LayoutParams(width / 3, height / 3);
        layoutParams1.setMargins(width / 2 - PX9mm, 2 * height / 3, 0, 0);
        buttom.setLayoutParams(layoutParams1);
    }

    private void Vertical_Crosstalk_02() {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(width / 3, height / 3);

        layoutParams.setMargins(width / 2 + PX9mm, 0, 0, 0);
        top.setLayoutParams(layoutParams);

        RelativeLayout.LayoutParams layoutParams1 = new RelativeLayout.LayoutParams(width / 3, height / 3);
        layoutParams1.setMargins(width / 2 + PX9mm, 2 * height / 3, 0, 0);
        buttom.setLayoutParams(layoutParams1);
    }

    private void Vertical_Crosstalk_03() {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(width / 3, height / 3);

        layoutParams.setMargins(width / 6 + PX9mm, 0, 0, 0);
        top.setLayoutParams(layoutParams);

        RelativeLayout.LayoutParams layoutParams1 = new RelativeLayout.LayoutParams(width / 3, height / 3);
        layoutParams1.setMargins(width / 6 + PX9mm, 2 * height / 3, 0, 0);
        buttom.setLayoutParams(layoutParams1);
    }

    private void Vertical_Crosstalk_04() {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(width / 3, height / 3);

        layoutParams.setMargins(width / 6 - PX9mm, 0, 0, 0);
        top.setLayoutParams(layoutParams);

        RelativeLayout.LayoutParams layoutParams1 = new RelativeLayout.LayoutParams(width / 3, height / 3);
        layoutParams1.setMargins(width / 6 - PX9mm, 2 * height / 3, 0, 0);
        buttom.setLayoutParams(layoutParams1);

    }

    private void getInfo() {
        {
            DisplayMetrics dm = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(dm);

// will either be DENSITY_LOW, DENSITY_MEDIUM or DENSITY_HIGH
            float DPI = dm.densityDpi;
            float inch = (float) 25.40;
            float onePXmm = inch / DPI;
            PX9mm = Math.round(9 / onePXmm);

// Get screen size
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                display.getRealSize(size);
            }
            width = size.x;
            height = size.y;

            DisplayMetrics dm1 = new DisplayMetrics();
            A_Crosstalk.this.getWindowManager().getDefaultDisplay().getMetrics(dm1);
            int width = dm1.widthPixels;
            //height = dm1.heightPixels;
        }
    }

    // auto full screen again after stop activity
    @Override
    public void onResume() {
        super.onResume();
        setFullScreen();
    }

    @Override
    public void setFullScreen() {
        // init variables
        WindowManager.LayoutParams params = getWindow().getAttributes();
        View mDecor = this.getWindow().getDecorView();


        // set maximum brightness
        if (MainActivity.autoBrightness) {
            params.screenBrightness = WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_FULL;
        }

        // set screen always on
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // set full screen
        mDecor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                | View.SYSTEM_UI_FLAG_IMMERSIVE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            params.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
        }
        getWindow().setAttributes(params);
    }
}
