package vsmart.ngocdd3.screentest.activity.display;

import android.Manifest;
import android.app.Service;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import vsmart.ngocdd3.screentest.R;
import vsmart.ngocdd3.screentest.interfaces.I_MainFunctions;

public class A_Auto_Brightness extends AppCompatActivity implements I_MainFunctions {

    public static TextView txt_app_brightness, txt_light_sensor, label_brightness_level, label_sensor_values, txt_delayTime, txt_pcb_thermal, label_pcb_thermal;
    View autobrightness_layout;
    SensorManager sensorManager;
    Sensor sensor;
    Button btn_start;

    public static boolean disappear = false, getEndTime = false, getStartTime = false, getCondtion = false;


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__auto__brightness);

        // init
        init();

        // set full screen and keep screen on
        setFullScreen();

        // click action
        setOnClick();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.vsmartscreentest.THERMALPCA");

    } //end onCreate


    void setOnClick() {

        autobrightness_layout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                if (disappear) {
                    label_pcb_thermal.setVisibility(View.GONE);
                    label_brightness_level.setVisibility(View.GONE);
                    label_sensor_values.setVisibility(View.GONE);

                    txt_app_brightness.setVisibility(View.GONE);
                    txt_light_sensor.setVisibility(View.GONE);
                    txt_pcb_thermal.setVisibility(View.GONE);
                    txt_delayTime.setVisibility(View.GONE);

                    btn_start.setVisibility(View.GONE);
                    disappear = false;
                } else {
                    label_pcb_thermal.setVisibility(View.VISIBLE);
                    label_brightness_level.setVisibility(View.VISIBLE);
                    label_sensor_values.setVisibility(View.VISIBLE);

                    txt_app_brightness.setVisibility(View.VISIBLE);
                    txt_light_sensor.setVisibility(View.VISIBLE);
                    txt_pcb_thermal.setVisibility(View.VISIBLE);
                    txt_delayTime.setVisibility(View.VISIBLE);

                    label_pcb_thermal.setText("PCBA Thermal: ");
                    label_brightness_level.setText("Screen Brightness: ");
                    label_sensor_values.setText("Sensor Level: ");


                    btn_start.setVisibility(View.VISIBLE);
                    disappear = true;
                }
                return true;
            }
        });

        autobrightness_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFullScreen();
            }
        });

        btn_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getCondtion = true;
                getEndTime = true;
                getStartTime = true;

            }
        });
    }


    void init() {
        // init UI
        sensorManager = (SensorManager) getSystemService(Service.SENSOR_SERVICE);
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        txt_app_brightness = findViewById(R.id.txt_brightness_level);
        txt_light_sensor = findViewById(R.id.txt_light_sensor);
        label_brightness_level = findViewById(R.id.label_brightness_level);
        label_sensor_values = findViewById(R.id.label_sensor_values);
        autobrightness_layout = findViewById(R.id.autobrightness_layout);
        txt_delayTime = findViewById(R.id.txt_delayTime);
        btn_start = findViewById(R.id.btn_start);
        txt_pcb_thermal = findViewById(R.id.txt_pcb_thermal);
        label_pcb_thermal = findViewById(R.id.label_pcb_thermal);


        //set background color
        autobrightness_layout.setBackgroundColor(Color.rgb(255, 255, 255));
        disappear = true;


        if (ContextCompat.checkSelfPermission(
                this, Manifest.permission.WRITE_SETTINGS) ==
                PackageManager.PERMISSION_GRANTED) {
            // do somethings.

            // turn off adaptive brightness
            Settings.System.putInt(getContentResolver(),
                    Settings.System.SCREEN_BRIGHTNESS_MODE, Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC);

        }

    }


    @Override
    protected void onPause() {
        super.onPause();
        if (ContextCompat.checkSelfPermission(
                this, Manifest.permission.WRITE_SETTINGS) ==
                PackageManager.PERMISSION_GRANTED) {
            // do somethings.

            // turn off adaptive brightness
            Settings.System.putInt(getContentResolver(),
                    Settings.System.SCREEN_BRIGHTNESS_MODE, Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);

        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        setFullScreen();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void setFullScreen() {
        // init variables
        WindowManager.LayoutParams params = getWindow().getAttributes();
        View mDecor = this.getWindow().getDecorView();

        // set screen always on
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // set full screen
        mDecor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                | View.SYSTEM_UI_FLAG_IMMERSIVE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            params.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
        }
        getWindow().setAttributes(params);
    }

}
