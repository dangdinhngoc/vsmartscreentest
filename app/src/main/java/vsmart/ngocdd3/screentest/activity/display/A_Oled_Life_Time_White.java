package vsmart.ngocdd3.screentest.activity.display;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;


import vsmart.ngocdd3.screentest.R;
import vsmart.ngocdd3.screentest.activity.MainActivity;
import vsmart.ngocdd3.screentest.interfaces.I_MainFunctions;

public class A_Oled_Life_Time_White extends AppCompatActivity implements I_MainFunctions {

    View ALblack;
    View ALwhite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oled_life_time_white);

        // init
        ALwhite = findViewById(R.id.ALwhite);
        ALblack = findViewById(R.id.ALblack);

        ALwhite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ALwhite.setBackgroundColor(Color.rgb(255, 255, 255));
                ALwhite.setBackgroundColor(Color.rgb(0, 0, 0));
            }
        });
        ALblack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ALwhite.setBackgroundColor(Color.rgb(255, 255, 255));
                ALwhite.setBackgroundColor(Color.rgb(0, 0, 0));
            }
        });

        new CountDownTimer(864000000, 1000) {

            public void onTick(long millisUntilFinished) {
                System.out.println("Time Left: " + millisUntilFinished / 3600000);
//                textView.setText("Time Left: " + millisUntilFinished / 3600000 );
            }

            public void onFinish() {
//                textView.setText( "done!" );
            }
        }.start();

        setFullScreen();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getActionMasked();

        switch (action) {
            case MotionEvent.ACTION_DOWN:
                break;
            case MotionEvent.ACTION_MOVE:
                setFullScreen();
                break;
            case MotionEvent.ACTION_UP:
                setFullScreen();

            case MotionEvent.ACTION_CANCEL:
                // Return a VelocityTracker object back to be re-used by others.
                break;
        }
        return true;
    }

    @Override
    public void setFullScreen() {
        // init variables
        WindowManager.LayoutParams params = getWindow().getAttributes();
        View mDecor = this.getWindow().getDecorView();


        // set maximum brightness
        if (MainActivity.autoBrightness) {
            params.screenBrightness = WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_FULL;
        }

        // set screen always on
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // set full screen
        mDecor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                | View.SYSTEM_UI_FLAG_IMMERSIVE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            params.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
        }
        getWindow().setAttributes(params);
    }
}
