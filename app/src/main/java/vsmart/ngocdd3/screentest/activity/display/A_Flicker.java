package vsmart.ngocdd3.screentest.activity.display;

import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;


import vsmart.ngocdd3.screentest.R;
import vsmart.ngocdd3.screentest.activity.MainActivity;
import vsmart.ngocdd3.screentest.interfaces.I_MainFunctions;

public class A_Flicker extends AppCompatActivity implements I_MainFunctions {

    ImageView layout_flicker;
    float start_Point, end_Point;
    int fickerParrtern = 1;
    int bitmapHeight;
    int bitmapWidth;
    Toast toast;
    int screen_Width;
    int screen_Height;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__flicker);
        layout_flicker = findViewById(R.id.flicker_imageview);

        toast = new Toast(getApplicationContext());
        getScreenResolution();

        BitmapDrawable bitmap = (BitmapDrawable) this.getResources().getDrawable(R.drawable.flicker_1);
        bitmapHeight = bitmap.getBitmap().getHeight();
        bitmapWidth = bitmap.getBitmap().getWidth();

        layout_flicker.getLayoutParams().height = bitmapHeight;
        layout_flicker.getLayoutParams().width = bitmapWidth;

    }// End onCreate


    private void getScreenResolution() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getRealSize(size);
            screen_Width = size.x;
            screen_Height = size.y;
        }
    }

    @Override
    public void setFullScreen() {
        // init variables
        WindowManager.LayoutParams params = getWindow().getAttributes();
        View mDecor = this.getWindow().getDecorView();


        // set maximum brightness
        if (MainActivity.autoBrightness) {
            params.screenBrightness = WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_FULL;
        }

        // set screen always on
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // set full screen
        mDecor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                | View.SYSTEM_UI_FLAG_IMMERSIVE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            params.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
        }
        getWindow().setAttributes(params);
    }


    private void setFlickerPattern(int fickerParrtern) {
        switch (fickerParrtern) {
            case 1:
                layout_flicker.setImageResource(R.drawable.flicker_1);
                toast.cancel();
                toast = Toast.makeText(getApplicationContext(), "flicker_1", Toast.LENGTH_SHORT);
                toast.show();
                break;
            case 2:
                layout_flicker.setImageResource(R.drawable.flicker_10a);
                toast.cancel();
                toast = Toast.makeText(getApplicationContext(), "flicker_10a", Toast.LENGTH_SHORT);
                toast.show();
                break;
            case 3:
                layout_flicker.setImageResource(R.drawable.flicker_11a);
                toast.cancel();
                toast = Toast.makeText(getApplicationContext(), "flicker_11a", Toast.LENGTH_SHORT);
                toast.show();
                break;
            case 4:
                layout_flicker.setImageResource(R.drawable.flicker_12a);
                toast.cancel();
                toast = Toast.makeText(getApplicationContext(), "flicker_12a", Toast.LENGTH_SHORT);
                toast.show();
                break;
            case 5:
                layout_flicker.setImageResource(R.drawable.flicker_13a);
                toast.cancel();
                toast = Toast.makeText(getApplicationContext(), "flicker_13a", Toast.LENGTH_SHORT);
                toast.show();
                break;
            case 6:
                layout_flicker.setImageResource(R.drawable.flicker_2a);
                toast.cancel();
                toast = Toast.makeText(getApplicationContext(), "flicker_2a", Toast.LENGTH_SHORT);
                toast.show();
                break;
            case 7:
                layout_flicker.setImageResource(R.drawable.flicker_2c);
                toast.cancel();
                toast = Toast.makeText(getApplicationContext(), "flicker_2c", Toast.LENGTH_SHORT);
                toast.show();
                break;
            case 8:
                layout_flicker.setImageResource(R.drawable.flicker_3);
                toast.cancel();
                toast = Toast.makeText(getApplicationContext(), "flicker_3", Toast.LENGTH_SHORT);
                toast.show();
                break;
            case 9:
                layout_flicker.setImageResource(R.drawable.flicker_4a);
                toast.cancel();
                toast = Toast.makeText(getApplicationContext(), "flicker_4a", Toast.LENGTH_SHORT);
                toast.show();
                break;
            case 10:
                layout_flicker.setImageResource(R.drawable.flicker_5);
                toast.cancel();
                toast = Toast.makeText(getApplicationContext(), "flicker_5", Toast.LENGTH_SHORT);
                toast.show();
                break;
            case 11:
                layout_flicker.setImageResource(R.drawable.flicker_6a);
                toast.cancel();
                toast = Toast.makeText(getApplicationContext(), "flicker_6a", Toast.LENGTH_SHORT);
                toast.show();
                break;
            case 12:
                layout_flicker.setImageResource(R.drawable.flicker_7b);
                toast.cancel();
                toast = Toast.makeText(getApplicationContext(), "flicker_7b", Toast.LENGTH_SHORT);
                toast.show();
                break;
            case 13:
                layout_flicker.setImageResource(R.drawable.flicker_8a);
                toast.cancel();
                toast = Toast.makeText(getApplicationContext(), "flicker_8a", Toast.LENGTH_SHORT);
                toast.show();
                break;
            case 14:
                layout_flicker.setImageResource(R.drawable.flicker_9a);
                toast.cancel();
                toast = Toast.makeText(getApplicationContext(), "flicker_9a", Toast.LENGTH_SHORT);
                toast.show();
                break;
            case 15:
                layout_flicker.setImageResource(R.drawable.flicker__14);
                toast.cancel();
                toast = Toast.makeText(getApplicationContext(), "flicker__14", Toast.LENGTH_SHORT);
                toast.show();
                break;


        }
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int index = event.getActionIndex();
        int action = event.getActionMasked();

        switch (action) {
            case MotionEvent.ACTION_DOWN:
                start_Point = event.getX(index);

                break;
            case MotionEvent.ACTION_MOVE:
                break;
            case MotionEvent.ACTION_UP:

                end_Point = event.getX(index);
                System.out.println(end_Point);


                if ((start_Point - end_Point) > 5) {
                    if (fickerParrtern > 15) {
                        setFlickerPattern(fickerParrtern);
                        fickerParrtern = 1;
                    } else {
                        setFlickerPattern(fickerParrtern);
                        fickerParrtern++;
                    }
                }

                if ((start_Point - end_Point) < -5) {
                    if (fickerParrtern < 1) {
                        setFlickerPattern(fickerParrtern);
                        fickerParrtern = 1;
                    } else {
                        setFlickerPattern(fickerParrtern);
                        fickerParrtern--;
                    }
                }

                setFullScreen();
            case MotionEvent.ACTION_CANCEL:
                break;
        }
        return true;
    }


}
