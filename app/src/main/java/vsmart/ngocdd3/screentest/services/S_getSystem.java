package vsmart.ngocdd3.screentest.services;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.os.Build;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;

public class S_getSystem {

    public String keyThermal0, thermalZone_Number;
    HashMap allThermistor;



    public S_getSystem() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                findAllThermistor();
                thermalZone();
            }
        }).start();

    }

    public String thermalZone() {
        String command = "/sys/class/thermal/thermal_zone" + thermalZone_Number + "/temp";
        return command;
    }


    static String shellCat(String filePath) {
        try {
            Process process = Runtime.getRuntime().exec("cat " + filePath);
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line = stdInput.readLine();
            if (line == null) return "-0000";
            else return line;
        } catch (Exception e) {
            return "-0000";
        }
    }

    void getThermal() {
        String model = Build.DEVICE;
        switch (model) {
            case "V320A":
                keyThermal0 = (String) getKeyFromValue("skin-therm-adc");
                break;
            case "sugarcane": //v330
                keyThermal0 = (String) getKeyFromValue("xo-therm-step");
                break;
            case "V620A":
            case "V420A":
            case "casuarina"://V430
                if (Build.VERSION.RELEASE.equals("10")) {
                    keyThermal0 = (String) getKeyFromValue("vsm-pcb-therm-adc");
                } else {
                    keyThermal0 = (String) getKeyFromValue("quiet-therm-step");
                }
                break;
            case "v340u":
            case "jacaranda": //V740
            case "jacarandapro": //V741
            case "jacaranda5g": //V742
            case "juniper": //V441
            case "cassia": //V640
                keyThermal0 = (String) getKeyFromValue("vsm-pcb-therm-adc");
                break;
            case "v350u":
                keyThermal0 = (String) getKeyFromValue("quiet-therm-step");
                break;
            case "poinciana": //V750
                keyThermal0 = (String) getKeyFromValue("quiet-therm-usr");
                break;
        }

        // condition
//        thermalAlready = true;
    }


    //    // Hàm này để lấy index từ theo tên con trở truyền vào
    private Object getKeyFromValue(String value) {
        // load all Thermistor
        int thermal = 0;

        for (Object o : allThermistor.keySet()) {
            if (allThermistor.get(o).equals(value)) {
                thermalZone_Number = o.toString();
//                Log.e(TAG, "getKeyFromValue:" + value + " --> " + o.toString());
                return o.toString();
            }
        }
        return thermal;
    }

    private void findAllThermistor() {
//        Log.e(TAG, "findAllThermistor");
        allThermistor = new HashMap<>();
        try {
            for (int i = 0; i < 100; i++) {
//                MainActivity.notifyContent = i + "";
                String type = shellCat("/sys/class/thermal/thermal_zone" + i + "/type");
                if (type != "Off") {
                    allThermistor.put(i, type);
//                    Log.e(TAG, "thermalzone" + i + ", type: " + type);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // get command by model name
        getThermal();
    }



}
