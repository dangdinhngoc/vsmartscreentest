package vsmart.ngocdd3.screentest.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import vsmart.ngocdd3.screentest.R;
import vsmart.ngocdd3.screentest.activity.MainActivity;
import vsmart.ngocdd3.screentest.activity.display.A_Auto_Brightness;


public class S_Services extends Service implements SensorEventListener {

    // variables
    S_getSystem getThermalZone;
    NotificationCompat.Builder mBuilder;
    AsyncTask asyncTask;
    S_getSystem s_getSystem;
    A_Auto_Brightness a_auto_brightness;
    NotificationManagerCompat notificationManagerCompat;
    NotificationManager notificationManager;

    String CHANNEL_ID = "123";
    String CHANNEL_NAME = "ngocdd3";
    String CHANNEL_DESCIPTION = "";

    float startBrightness, startAmbientLight, startScreenBrightness, startTime, delaytime;
    private SensorManager mSensorManager = null;
    private Sensor mLightSensor = null;
    private float curAmbientLight, curScreenBrightness, curPCBATemperature;
    boolean serviceState = true;

    //get/set
    public float getDelaytime() {
        return delaytime;
    }

    public void setDelaytime(float delaytime) {
        this.delaytime = delaytime;
    }

    public float getCurAmbientLight() {
        return curAmbientLight;
    }

    public void setCurAmbientLight(float curAmbientLight) {
        this.curAmbientLight = curAmbientLight;
    }

    public float getCurScreenBrightness() {
        return curScreenBrightness;
    }

    public void setCurScreenBrightness(float curScreenBrightness) {
        this.curScreenBrightness = curScreenBrightness;
    }

    public float getCurPCBATemperature() {
        return curPCBATemperature;
    }

    public void setCurPCBATemperature(float curPCBATemperature) {
        this.curPCBATemperature = curPCBATemperature;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        init();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        serviceState = false;
        asyncTask.isCancelled();
        mSensorManager.unregisterListener(this);
//        notificationManager.cancelAll();
        notificationManagerCompat.cancelAll();
    }

    void init() {
        serviceState = true;
        asyncTask = new MyAssyncTask();
        getAppBrightness();
        getThermalZone = new S_getSystem();
        s_getSystem = new S_getSystem();
        createNotificationChannel();
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mLightSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        mSensorManager.registerListener(this, mLightSensor, SensorManager.SENSOR_DELAY_NORMAL);
        a_auto_brightness = new A_Auto_Brightness();

    }


    @Override
    public void onStart(Intent intent, int startId) {

    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = CHANNEL_NAME;
            String description = CHANNEL_DESCIPTION;
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);

            notificationManager.createNotificationChannel(channel);

            notificationManager.cancelAll();
        }
    }

    private void notifyThis(String message) {
        mBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(null)
                .setContentText(message)
                .setSound(null)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setOnlyAlertOnce(true)
                .setOngoing(true)
                .setWhen(System.currentTimeMillis());

        notificationManagerCompat = NotificationManagerCompat.from(this);
        Notification notification = mBuilder.getNotification();

        notification.flags |= Notification.FLAG_NO_CLEAR
                | Notification.FLAG_ONGOING_EVENT;

        // notificationId is a unique int for each notification that you must define

        notificationManagerCompat.notify(0, notification);


    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_LIGHT) {

            setCurAmbientLight(event.values[0]);
//            Log.d("HTN", getCurAmbientLight() + "");
            int threshold = MainActivity.txt_Step;

            if (threshold < 0) {
                threshold = 100;
            }

            float ambientlightchanged = getCurAmbientLight() - startScreenBrightness;

            if (ambientlightchanged < 0) {
                ambientlightchanged = ambientlightchanged * -1;
            }

            if (a_auto_brightness.getCondtion) {
                startAmbientLight = getCurAmbientLight();
            }

            if (a_auto_brightness.getStartTime && ambientlightchanged > threshold) {
                startTime = System.currentTimeMillis();
                a_auto_brightness.getStartTime = false;
            }

            if (a_auto_brightness.disappear) {
                a_auto_brightness.txt_light_sensor.setText(getCurAmbientLight() + " Lux");

            } else {
//                a_auto_brightness.txt_light_sensor.setText("");
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }


    // run adb command
    static String shellCat(String filePath) {
        try {
            Process process = Runtime.getRuntime().exec("cat " + filePath);
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line = stdInput.readLine();
            if (line == null) return "0000";
            else return line;
        } catch (Exception e) {
            return "-1";
        }
    }


    class MyAssyncTask extends AsyncTask<Object, Float, Void> {

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onProgressUpdate(Float... values) {
            if(serviceState){
                notifyThis("Temperature: " + values[0] / 1000 + " °C" + "  /  Light Sensor: " + values[1] + " Lux");

                if (a_auto_brightness.disappear) {
                    a_auto_brightness.txt_app_brightness.setText(values[3] + " -");
                    a_auto_brightness.txt_delayTime.setText(values[2] / 1000 + " ms");
                    a_auto_brightness.txt_pcb_thermal.setText(values[0] / 1000 + " C");
                }
            }

        }

        @Override
        protected Void doInBackground(Object[] objects) {

            // Run condtition
            while (serviceState) {
                try {
                    // set delay time for service
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                // get pcb thermal
                setCurPCBATemperature(Integer.parseInt(shellCat(getThermalZone.thermalZone())));
//                    Log.e(TAG, "thermal::::  " + thermal);
                // get screen brightness
                try {
                    setCurScreenBrightness(Settings.System.getInt(getContentResolver(), Settings.System.SCREEN_BRIGHTNESS));
//                    Log.e("HTN", "Cur Screen Brightness  " + getCurScreenBrightness() + "");

                    if (a_auto_brightness.getCondtion) {
                        startScreenBrightness = getCurScreenBrightness();
                        a_auto_brightness.getCondtion = false;
                    }
                    if (startScreenBrightness != getCurScreenBrightness()) {
                        if (a_auto_brightness.getEndTime) {
                            setDelaytime(System.currentTimeMillis() - startTime);

//                            Log.e("HTN", "delaytime  " + getDelaytime() + "");
                            startBrightness = getCurScreenBrightness();
                            a_auto_brightness.getEndTime = false;
                        }
                    }
                    // update information for UI
                    publishProgress(getCurPCBATemperature(), getCurAmbientLight(), (float) getDelaytime(), getCurScreenBrightness());
                    Intent thermalIntent = new
                            Intent("com.vsmartscreentest.THERMALPCA");
                    thermalIntent.putExtra("THERMAL", getCurPCBATemperature());
                    sendBroadcast(thermalIntent);

                } catch (Settings.SettingNotFoundException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

    }


    public void getAppBrightness() {

        asyncTask.execute();
    }

}
