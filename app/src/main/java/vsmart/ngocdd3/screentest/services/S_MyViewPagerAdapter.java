package vsmart.ngocdd3.screentest.services;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import vsmart.ngocdd3.screentest.fragment.F_Display;
import vsmart.ngocdd3.screentest.fragment.F_Home;
import vsmart.ngocdd3.screentest.fragment.F_Touch;

public class S_MyViewPagerAdapter extends FragmentPagerAdapter {
    public static final int NUM_PAGER = 3;

    public S_MyViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return F_Home.newInstance(0, "HOME");

            case 1:
                return F_Display.newInstance(1, "DISPLAY");

            case 2:
                return F_Touch.newInstance(2, "TOUCH");

            default:
                return F_Home.newInstance(0, "HOME");

        }
    }

    @Override
    public int getCount() {
        return NUM_PAGER;
    }


    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "HOME";
            case 1:
                return "DISPLAY";
            case 2:
                return "TOUCH";
            default:
                return "HOME";
        }
    }
}
