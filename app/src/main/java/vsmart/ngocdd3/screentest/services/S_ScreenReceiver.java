package vsmart.ngocdd3.screentest.services;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;

import vsmart.ngocdd3.screentest.activity.touch.A_Double_Tap_Success_Rate;

public class S_ScreenReceiver extends BroadcastReceiver {
    public static int ScreenOFF, ScreenON;



    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (Intent.ACTION_SCREEN_ON.equals(action)) {
            ScreenON += 1;
//            Log.d( "DB", "ON" );
            String numberAsString = String.valueOf(ScreenON);
            A_Double_Tap_Success_Rate.screenON.setText(numberAsString);


        } else if (Intent.ACTION_SCREEN_OFF.equals(action)) {
//            Log.d( "DB", "OFF" );
            ScreenOFF += 1;
            String numberAsString = String.valueOf(ScreenOFF);
            A_Double_Tap_Success_Rate.screenOFF.setText(numberAsString);
        }
    }

    public static void reset() {
        ScreenON = 0;
        ScreenOFF = 0;
    }



}